package symboltable;
import org.junit.Test;
import static org.junit.Assert.*;
import parser.Parser;

/**
 * Testing for integration of SymbolTable into Parser.
 * @author eddy
 */
public class SymbolTableIntegrationTest {
    
    ///////////////////////////////////////////
    //Parser Intergration of SymbolTable Tests
    ///////////////////////////////////////////
    /**
     * test to determine if a new parser loads a symbol table correctly.
     */
    @Test
    public void parserTableLoadTest() {
        String stringName = "";
        Parser parser = new Parser(stringName, false);
        SymbolTable symbolTable = new SymbolTable();

        assertEquals(parser.symbolTable.toString(), symbolTable.toString());
    }

    /**
     * passing test for assignment call.
     */
    @Test
    public void assignmentCallPassTest() {
        // Create string to use in our testing.
        String variable = "foo := 9 + 10";

        Parser variableParser = new Parser(variable, false);
        variableParser.symbolTable.addKind("foo", Kind.VARIABLE);
        variableParser.statement();
    }

    /**
     * passing test for procedure call.
     */
    @Test
    public void procedureCallPassTest() {
        // Create string to use in our testing.
        String procedure = "bar (1)";

        Parser procedureParser = new Parser(procedure, false);
        procedureParser.symbolTable.addKind("bar", Kind.PROCEDURE);
        procedureParser.statement();
    }

    /**
     * failing test for assignment call, note it will fail due to "(" 
     * if statement() works correctly.
     */
    @Test
    public void assingmentCallFailTest() {
        
        String procedure = "bar (1)";
        Parser parserC = new Parser(procedure, false);
        parserC.symbolTable.addKind("bar", Kind.VARIABLE);
        System.out.println(parserC.symbolTable);
        parserC.statement();
   
    }

}
