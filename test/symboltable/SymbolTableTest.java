package symboltable;

import org.junit.Test;
import static org.junit.Assert.*;
import parser.Parser;

/**
 * Testing for functions in Symbol Table Tests
 *
 * @author eddy
 */
public class SymbolTableTest {

    /**
     * testing symbol table PROGRAM ID kind by comparing values that we add and
     * read from SymbolTable.
     */
    @Test
    public void getKindTest() {
        //first create a symbol table to test with. 
        SymbolTable symbolTable = new SymbolTable();
        //add a kind of id into table
        symbolTable.addKind("foo", Kind.PROGRAM);//also tests for add kind.
        symbolTable.addKind("fo", Kind.ARRAY);
        Kind readKind = symbolTable.getKind("foo");//should return program kind

        Kind expected = Kind.PROGRAM;
        assertEquals(expected, readKind);//this test should pass with no errors.
    }

    /**
     * testing symbol table for recognition of null kinds for non existent
     * variables.
     */
    @Test
    public void getKindNullTest() {
        //first create a symbol table to test with. 
        SymbolTable symbolTable = new SymbolTable();
        //add a kind of id into table
        symbolTable.addKind("foo", Kind.PROGRAM);//also tests for add kind.
        symbolTable.addKind("fo", Kind.ARRAY);
        Kind readKind = symbolTable.getKind("f");//should return null kind

        Kind expected = null;
        assertEquals(expected, readKind);//this test should pass with no errors.
    }

    /**
     * testing of isKind() function in SymbolTable class here we will test to
     * see if true is returned when expected.
     */
    @Test
    public void isKindTrueTest() {
        //first create a symbol table to test with. 
        SymbolTable symbolTable = new SymbolTable();
        //add a kind of id into table
        symbolTable.addKind("foo", Kind.VARIABLE);
        symbolTable.addKind("fo", Kind.ARRAY);
        boolean returned = symbolTable.isKind("foo", Kind.VARIABLE);//true.

        boolean expected = true;
        assertEquals(expected, returned);//this test should pass with no errors.
    }

    /**
     * testing of isKind() function in SymbolTable class here we will test to
     * see if true is returned when expected.
     */
    @Test
    public void isKindFailTest() {
        //first create a symbol table to test with. 
        SymbolTable symbolTable = new SymbolTable();
        //add a kind of id into table
        symbolTable.addKind("foo", Kind.VARIABLE);
        symbolTable.addKind("fo", Kind.ARRAY);
        boolean returned = symbolTable.isKind("fo", Kind.VARIABLE);//false

        boolean expected = false;
        assertEquals(expected, returned);//this test should pass with no errors.
    }

}
