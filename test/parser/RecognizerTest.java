package parser;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import scanner.TokenType;

/**
 *JUnit testing class for Recognizer class
 *functions such as program, declarations, 
 * subprogram_declaration, statement, 
 * simple_expression, factor.
 *@author Eduardo Bautista
 */
public class RecognizerTest {

    /**
     * jUnit pass and fail
     * test for program.
     */
    @Test
    public void testProgram(){
        String programPassString = 
            "program"
            + "	foo"
            + "	;"
            + "	begin"
            + "	end"
            + "	.";
    
        String programFailString = 
            "program"
            + "	;"
            + "	foo"
            + "	begin"
            + "	end"
            + "	.";
    
        //testing for no error program string.
        Recognizer recognizer = new Recognizer(programPassString, false);
        try  {
            recognizer.program();
        
        }
        catch(Exception e){ //catching all exceptions for now.
            fail("fail"); // here we write the fail.   
        }
        //testing for error program string.
        recognizer = new Recognizer(programFailString, false);
        try  {
            recognizer.program();   
        }
        catch(Exception e){ //catching all exceptions for now.
            assertEquals(null, e.getMessage()); 
        }  

    }
    
    
    /**
     * jUnit pass and fail
     * test for declarations.
     */
    @Test
    public void testDeclarations(){
    String declarationPassString = 
            "var"
            + "	foo"
            + "	:"
            + "	real"
            + "	;"
            + "	var"    
            + "	fo" 
            + "	:"    
            + "	integer"
            + "	;";    
    
    String declarationFailString = 
            "var"
            + "	65"
            + "	;"
            + "	real"
            + "	:"
            + "	var"    
            + "	fo" 
            + "	;"    
            + "	integer"
            + "	:";         
        //testing for no error program string.
        Recognizer recognizer = new Recognizer(declarationPassString, false);
        try  {
            recognizer.declarations();
        
        }
        catch(Exception e){ //catching all exceptions for now.
            fail("fail"); // here we write the fail.
        
        }
        //testing for error program string.
        recognizer = new Recognizer(declarationFailString, false);
        try  {
            recognizer.declarations();
        
        }
        catch(Exception e){ //catching all exceptions for now.
            assertEquals(null, e.getMessage()); 
        }    
    }
    /**
     * jUnit pass and fail
     * test for 
     * subprogram_declaration.
     */
    @Test
    public void TestSubprogram_declaration(){
    String subprogram_declarationPassString = 
            "procedure"
            + "	foo"
            + "	;"
            + "	var"
            + "	fo"
            + "	:"    
            + "	integer" 
            + "	;"    
            + "	begin"
            + "	end";            
    
    String subprogram_declarationFailString = 
            "function"
            + "	34"
            + "	;"
            + "	var"
            + "	fo"
            + "	;"    
            + "	integer" 
            + "	;"    
            + "	end"
            + "	begin";          
    
    
        //testing for no error sup program declaration string.
        Recognizer recognizer = new Recognizer(subprogram_declarationPassString, false);
        try  {
            recognizer.subprogram_declarations();
        
        }
        catch(Exception e){ //catching all exceptions for now.
            fail("fail"); // here we write the fail.
        
        }
        //testing for error string.
        recognizer = new Recognizer(subprogram_declarationFailString, false);
        try  {
            recognizer.subprogram_declarations();
        
        }
        catch(Exception e){ //catching all exceptions for now.
            assertEquals(null, e.getMessage()); 
        }     
    }
    
    /**
     * jUnit pass and fail
     * test for Statement.
     */
    @Test
    public void testStatement(){
        String statementPassString = 
            "foo"
            + "	:="
            + "	fo"
            + "	*"    
            + "	1";
    
        String statementFailString = 
            "procedure"
            + "	foo"
            + "	fi"
            + "	="
            + "	fo"
            + "	*"    
            + "	1";
    
        //test for passing string
        Recognizer recognizer = new Recognizer(statementPassString, false);
        try  {
            recognizer.statement();
        
        }
        catch(Exception e){ //catching all exceptions for now.
            fail(e.getMessage()); // here we write the fail.
        
        }
        
        //testing for error string.
        recognizer = new Recognizer(statementFailString, false);
        try  {
            recognizer.statement();
            fail("fail test");
        
        }
        catch(Exception e){ //catching all exceptions for now.
            assertEquals(null, e.getMessage()); 
        }         
    }
    /**
     * jUnit pass and fail
     * test for
     * simple_expression.
     */
    @Test
    public void simple_expression(){
        String simple_expPassString = 
            "foo"
            + "	*"
            + "	3"
            + "	+"    
            + "	fo";

        String simple_expressionFailString = 
            "procedure"
            + "	foo"
            + "	3"
            + "	*"
            + "	fo"    
            + "	1";
    
        //test for passing string
        Recognizer recognizer = new Recognizer(simple_expPassString,false);
        try  {
            recognizer.simple_expression();
        
        }
        catch(Exception e){ //catching all exceptions for now.
            fail(e.getMessage()); // here we write the fail.
        
        }
        
        //testing for error string.
        recognizer = new Recognizer(simple_expressionFailString, false);
        try  {
            recognizer.simple_expression();
            fail("fail test");
        
        }
        catch(Exception e){ //catching all exceptions for now.
            assertEquals(null, e.getMessage()); 
        }         
        
    }
    /**
     * jUnit pass and fail
     * test for factors.
     */
    @Test
    public void factor(){
        String factorPassString = 
            "45";

        String factorFailString = 
            "foo"
            + "	("
            + "	45";
    
        //test for passing string
        Recognizer recognizer = new Recognizer(factorPassString,false);
        try  {
            recognizer.simple_expression();
        
        }
        catch(Exception e){ //catching all exceptions for now.
            fail(e.getMessage()); // here we write the fail.
        
        }
        
        //testing for error string.
        recognizer = new Recognizer(factorFailString, false);
        try  {
            recognizer.simple_expression();
            fail("fail test");
        
        }
        catch(Exception e){ //catching all exceptions for now.
            assertEquals(null, e.getMessage()); 
        }            
    }
} //class end
    
    
    


