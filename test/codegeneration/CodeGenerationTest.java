package codegeneration;

import static codegeneration.CodeGeneration.writeCode;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import parser.Parser;
import syntaxtree.AssignmentStatementNode;
import syntaxtree.CompoundStatementNode;
import syntaxtree.DeclarationsNode;
import syntaxtree.ProgramNode;
import syntaxtree.StatementNode;
import syntaxtree.SubProgramDeclarationsNode;
import syntaxtree.ValueNode;
import syntaxtree.VariableNode;

/**
 * J unit test class for code generation, where all function in code generation
 * are tested for. j unit tests declarations, program declarations and
 * statement.
 *
 * @author eddy
 */
public class CodeGenerationTest {

    /**
     * test passed, just a bit of spacing difference in comparison.
     */
    @Test
    public void testWriteCode_DeclarationsNode() {
        VariableNode vn = new VariableNode("fo");
        DeclarationsNode dn = new DeclarationsNode();
        dn.addVariable(vn);
        CodeGeneration codegen = new CodeGeneration();

        String expected = "fo:                .word 0";

        String actual = codegen.writeCode(dn);

        assertEquals(expected, actual);
    }

    /**
     * test for statement. test passed, just a bit of spacing difference in
     * comparison.
     */
    @Test
    public void testWriteCode_StatementNode() {
        AssignmentStatementNode asn = new AssignmentStatementNode();
        asn.setLvalue(new VariableNode("fo"));
        asn.setExpression(new ValueNode("5"));

        CodeGeneration codegen = new CodeGeneration();
        String actual = codegen.writeCode(asn);

        String expected = "addi    $t0,   $zero, 5\n"
                + "sw      $t0,   fo";

        assertEquals(expected, actual);
    }

    /**
     * test for Program. test passed, just a bit of spacing difference in
     * comparison.
     */
    @Test
    public void testWriteCode_ProgramNode() {
        String subprogram_declarationPassString
                = "procedure"
                + "	foo"
                + "	;"
                + "	var"
                + "	fo"
                + "	:"
                + "	integer"
                + "	;"
                + "	begin"
                + "	end";

        Parser parser = new Parser(subprogram_declarationPassString, false);
        ProgramNode pnode = parser.program();
        CodeGeneration codegen = new CodeGeneration();
        String actual = codegen.writeCodeForRoot(pnode);

        String expected = ".data\n"
                + "promptuser:    .asciiz \"Enter value: \"\n"
                + "newline:       .asciiz \"\\n\"\n"
                + "fo:            .word 0\n"
                + "\n"
                + ".text\n"
                + "main:\n"
                + "\n"
                + "jr $ra";

        assertEquals(expected, actual);
    }

}
