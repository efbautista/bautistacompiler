package analyzer;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import syntaxtree.ProgramNode;

import syntaxtree.*;
import parser.*;
import analyzer.*;
import scanner.*;

/**
 *
 * @author eddy
 */
public class AnalyzerTest {
    /**
     * Test of declaredVariableCheck, this test will pass because we are assuming
     * flagged means they were flagged.
     */
    @Test
    public void testVariablesDeclaredCheckNotFlagged(){
        System.out.println("-----test : Not Flagged-------");
        //note bar is not declared before usage on here.
        String test = "program test;" +
                //"var  fo : integer;" +
                "begin" +
                "   fo := 2 - 1" +
                "end.";
        Parser p = new Parser(test, false);
        
        Analyzer analyzer = new Analyzer(p.program());
        analyzer.Analyze();
        //should return false.
        boolean actual = analyzer.getCodeError();
        //if true, it means variables were used without being declared.
        boolean expected = true;

        //currently always returning true.
        assertEquals(expected, actual);

    }
}

