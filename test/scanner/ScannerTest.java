package scanner;
import java.io.StringReader;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *Test class for the nextToken function of scanner function produced through jflex.
 *@author Eduardo Flores
 */
public class ScannerTest {

    /**
     * Test of nextToken method of class Scanner.
     */
    @Test
    public void testNextToken() {
    String inString =
            "begin"
            + "	end"
            + "	myId123" //identifier (id)
            + "	%"
            + "	/"
            + "	:="
            + "	*"
            + "	write"
            + "	read"
            + "	["
            + "	]"
            + "	("
            + "	)"
            + "	<"
            + "	>"
            + "	<="
            + "	>="
            + "	procedure"
            + "	program"
            + "	real"
            + "	then"
            + "	:"
            + "	."
            + "	,"
            + "	;"
            + "	while"
            + "	var"
            + "	begin"
            + "	div"
            + "	do"
            + "	or"
            + "	not"
            + "	mod"
            + "	integer"
            + "	if"
            + "	else"
            + "	function"
            + "	array"
            + "	<>"
            + "	of"
            + "	+"
            + "	-"
            + "	="
            + "	return";
    
    
    Scanner instance = new Scanner (new StringReader (inString));
    Token myToken = null;
       
    //Token Test for BEGIN Token
    System.out.println("BEGIN token test");
    TokenType expected = TokenType.BEGIN;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for END Token
    System.out.println("END token test");
    expected = TokenType.END;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for ID Token
    System.out.println("ID token test");
    expected = TokenType.ID;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    
    //Token Test for ILLEGAL Token
    System.out.println("ILLEGAL token test");
    expected = TokenType.ILLEGAL;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    
    //Token Test for SLASH Token
    System.out.println("SLASH token test");
    expected = TokenType.SLASH;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for BECOMES Token
    System.out.println("BECOMES token test");
    expected = TokenType.BECOMES;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for ASTERISK Token
    System.out.println("ASTERISK token test");
    expected = TokenType.ASTERISK;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    //Token Test for WRITE Token
    System.out.println("WRITE token test");
    expected = TokenType.WRITE;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    //Token Test for READ Token
    System.out.println("READ token test");
    expected = TokenType.READ;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for LSBRACKET Token
    System.out.println("LSBRACKET token test");
    expected = TokenType.LSBRACKET;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    //Token Test for RSBRACKET Token
    System.out.println("RSBRACKET token test");
    expected = TokenType.RSBRACKET;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    //Token Test for LPARENTHESES Token
    System.out.println("LPARENTHESES token test");
    expected = TokenType.LPARENTHESES;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for RPARENTHESES Token
    System.out.println("RPARENTHESES token test");
    expected = TokenType.RPARENTHESES;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for LESSTHAN Token
    System.out.println("LESSTHAN token test");
    expected = TokenType.LESSTHAN;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    
    //Token Test for GREATERTHAN Token
    System.out.println("GREATERTHAN token test");
    expected = TokenType.GREATERTHAN;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    //Token Test for LESSTHANOREQUALTO Token
    System.out.println("LESSTHANOREQUALTO token test");
    expected = TokenType.LESSTHANOREQUALTO;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    //Token Test for GREATERTHANOREQUALTO Token
    System.out.println("GREATERTHANOREQUALTO token test");
    expected = TokenType.GREATERTHANOREQUALTO;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}

 
    //Token Test for PROCEDURE Token
    System.out.println("PROCEDURE token test");
    expected = TokenType.PROCEDURE;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    //Token Test for PROGRAM Token
    System.out.println("PROGRAM token test");
    expected = TokenType.PROGRAM;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    

    //Token Test for REAL Token
    System.out.println("REAL token test");
    expected = TokenType.REAL;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for THEN Token
    System.out.println("THEN token test");
    expected = TokenType.THEN;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for COLON Token
    System.out.println("COLON token test");
    expected = TokenType.COLON;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for PERIOD Token
    System.out.println("PERIOD token test");
    expected = TokenType.PERIOD;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for COMMA Token
    System.out.println("COMMA token test");
    expected = TokenType.COMMA;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
   
    //Token Test for SEMICOLON Token
    System.out.println("SEMICOLON token test");
    expected = TokenType.SEMICOLON;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for WHILE Token
    System.out.println("WHILE token test");
    expected = TokenType.WHILE;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for VAR Token
    System.out.println("VAR token test");
    expected = TokenType.VAR;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for BEGIN Token
    System.out.println("BEGIN token test");
    expected = TokenType.BEGIN;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for DIV Token
    System.out.println("DIV token test");
    expected = TokenType.DIV;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for DO Token
    System.out.println("DO token test");
    expected = TokenType.DO;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for OR Token
    System.out.println("OR token test");
    expected = TokenType.OR;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for NOT Token
    System.out.println("NOT token test");
    expected = TokenType.NOT;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for MOD Token
    System.out.println("MOD token test");
    expected = TokenType.MOD;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for INTEGER Token
    System.out.println("INTEGER token test");
    expected = TokenType.INTEGER;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for IF Token
    System.out.println("IF token test");
    expected = TokenType.IF;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for ELSE Token
    System.out.println("ELSE token test");
    expected = TokenType.ELSE;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for FUNCTION Token
    System.out.println("FUNCTION token test");
    expected = TokenType.FUNCTION;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    

    //Token Test for ARRAY Token
    System.out.println("ARRAY token test");
    expected = TokenType.ARRAY;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for NOTEQUAL Token
    System.out.println("NOTEQUAL token test");
    expected = TokenType.NOTEQUAL;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for OF Token
    System.out.println("OF token test");
    expected = TokenType.OF;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    

    //Token Test for PLUS Token
    System.out.println("PLUS token test");
    expected = TokenType.PLUS;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for MINUS Token
    System.out.println("MINUS token test");
    expected = TokenType.MINUS;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    
    //Token Test for EQUAL Token
    System.out.println("EQUAL token test");
    expected = TokenType.EQUAL;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    //Token Test for RETURN Token
    System.out.println("RETURN token test");
    expected = TokenType.RETURN;
    
    try {
    myToken = instance.nextToken();
    assertEquals(expected, myToken.getType());
    }
    catch( Exception e) { e.printStackTrace();}
    
    } 
}