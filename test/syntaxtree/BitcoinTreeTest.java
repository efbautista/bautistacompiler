package syntaxtree;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import scanner.TokenType;

/**
 * J unit standalone test for SyntaxTree with bitcoin example.
 * @author Eduardo Flores
 */
public class BitcoinTreeTest {
    /**
     * builds a full tree such as the bitcoin example, and test the
     * indentedToString() method.
     */
    @Test
    public void testBitcoinExample() {
        ProgramNode pNode = new ProgramNode("sample");
        DeclarationsNode dNode = new DeclarationsNode();
        //curency variable nodes for dollars, yen and bitcoin.
        VariableNode dollars = new VariableNode("dollars");
        VariableNode yen = new VariableNode("yen");
        VariableNode bitcoins = new VariableNode("bitcoins");
        // Adding variable Nodes to dNode.
        dNode.addVariable(dollars);
        dNode.addVariable(yen);
        dNode.addVariable(bitcoins);
        // Adding dNodes variable to pNode.
        pNode.setVariables(dNode);

        //subprogramDeclarations and compoundStatement nodes.
        SubProgramDeclarationsNode sPDNode = new SubProgramDeclarationsNode();
        pNode.setFunctions(sPDNode);
        CompoundStatementNode cSNode = new CompoundStatementNode();

        AssignmentStatementNode aNodeUSD = new AssignmentStatementNode();
        //lvalue of dollars to assignment node aNodeUSD.
        aNodeUSD.setLvalue(dollars);
        //expression of 1000000 to assignment node aNodeUSD.
        aNodeUSD.setExpression(new ValueNode("1000000"));

        AssignmentStatementNode aNodeYen = new AssignmentStatementNode();
        //lvalue of yen to assignment node.
        aNodeYen.setLvalue(yen);
        //creating a multiply(*) oppperationsNode
        OperationNode yenMultiply = new OperationNode(TokenType.ASTERISK);
        yenMultiply.setLeft(dollars);
        yenMultiply.setRight(new ValueNode("110"));
        //expression of yenMultiply to assignment node aNodeYen.
        aNodeYen.setExpression(yenMultiply);

        AssignmentStatementNode aNodeBitcoins = new AssignmentStatementNode();
        //lvalue of bitcoins to assignment node.
        aNodeBitcoins.setLvalue(bitcoins);
        //creating a divide(/) oppperationsNode
        OperationNode bitcoinsDivide = new OperationNode(TokenType.SLASH);
        bitcoinsDivide.setLeft(dollars);
        bitcoinsDivide.setRight(new ValueNode("3900"));
        //expression of bitcoinsDivide to assignment node aNodeBitcoins.
        aNodeBitcoins.setExpression(bitcoinsDivide);

        //add statments to compoundStatmentsNode.
        cSNode.addStatement(aNodeUSD);
        cSNode.addStatement(aNodeYen);
        cSNode.addStatement(aNodeBitcoins);

        pNode.setMain(cSNode);

        //actual
        String actualString = pNode.indentedToString(0);

        //expected
        String expectedString = "Program: sample\n"
                + "|-- Declarations\n"
                + "|-- --- Name: dollars\n"
                + "|-- --- Name: yen\n"
                + "|-- --- Name: bitcoins\n"
                + "|-- SubProgramDeclarations\n"
                + "|-- Compound Statement\n"
                + "|-- --- Assignment\n"
                + "|-- --- --- Name: dollars\n"
                + "|-- --- --- Value: 1000000\n"
                + "|-- --- Assignment\n"
                + "|-- --- --- Name: yen\n"
                + "|-- --- --- Operation: ASTERISK\n"
                + "|-- --- --- --- Name: dollars\n"
                + "|-- --- --- --- Value: 110\n"
                + "|-- --- Assignment\n"
                + "|-- --- --- Name: bitcoins\n"
                + "|-- --- --- Operation: SLASH\n"
                + "|-- --- --- --- Name: dollars\n"
                + "|-- --- --- --- Value: 3900\n";


        assertEquals(expectedString, actualString);

    }
}
