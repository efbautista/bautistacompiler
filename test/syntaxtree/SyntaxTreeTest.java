package syntaxtree;

import parser.Parser;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;
import scanner.TokenType;
import symboltable.Kind;
import syntaxtree.*;

/**
 * J unit standalone test for SyntaxTree.
 *
 * @author Eduardo Flores
 */
public class SyntaxTreeTest {

    /**
     * factor value indentedToString test.
     */
    @Test
    public void testFactorValue() {
        ValueNode valueNode = new ValueNode("0");
        String test = "0";
        Parser parser = new Parser(test, false);

        String expected = valueNode.indentedToString(0);
        String actual = parser.factor().indentedToString(0);

        assertEquals(expected, actual);
    }

    /**
     * factorindentedToString return test.
     */
    @Test
    public void testFactorVariable() {
        VariableNode variableNode = new VariableNode("foo");
        String test = "foo";
        Parser parser = new Parser(test, false);

        String expected = variableNode.indentedToString(0);
        String actual = parser.factor().indentedToString(0);

        assertEquals(expected, actual);
    }

    /**
     * simple_expression indentedToString return test.
     */
    @Test
    public void testSimpleExpression() {
        OperationNode plusNode = new OperationNode(TokenType.PLUS);
        OperationNode multiplyNode = new OperationNode(TokenType.ASTERISK);
        multiplyNode.setLeft(new ValueNode("2"));
        multiplyNode.setRight(new VariableNode("foo"));
        plusNode.setLeft(new ValueNode("1"));
        plusNode.setRight(multiplyNode);

        String test = "1 + 2 * foo";
        Parser p = new Parser(test, false);

        String expected = plusNode.indentedToString(0);
        String actual = p.simple_expression().indentedToString(0);

        assertEquals(expected, actual);
    }

    /**
     * statement indentedToString return test.
     */
    @Test
    public void testStatement() {
        AssignmentStatementNode asn = new AssignmentStatementNode();
        asn.setLvalue(new VariableNode("foo"));
        asn.setExpression(new ValueNode("5"));

        String expected = asn.indentedToString(0);
        String test = "foo := 5";

        Parser parser = new Parser(test, false);
        parser.symbolTable.addKind("foo", Kind.VARIABLE);

        String actual = parser.statement().indentedToString(0);

        assertEquals(expected, actual);
    }

    /**
     * subProgramDeclaration indentedToString return test.
     *//*
    @Test
    public void testSubProgramDeclarations() {

        String testString = "procedure foo (x : integer); var bar : integer; begin bar := x end;";
        Parser scanner = new Parser(testString, false);
        try {
            // Building the  DeclarationsNode
            SubProgramDeclarationsNode spdn = scanner.subprogram_declaration(new SubProgramDeclarationsNode());

            System.out.println(spdn.indentedToString(0));
            String expectedString = "SubProgramDeclarations\n";

            assertEquals(expectedString, spdn.indentedToString(0));
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }
*/
    /**
     * Declaration indentedToString return test.
     */
    @Test
    public void testDeclarations() {
        String testString = "var declaration, foo, bar : real ;";

        // Constructor to build parse tree 
        Parser scanner = new Parser(testString, false);

        try {
            // Build a DeclarationsNode by running declarations() 
            DeclarationsNode declarationsNode = scanner.declarations(new DeclarationsNode());

            // Prints out the indentedToString 
            System.out.println(declarationsNode.indentedToString(0));

            String expectedString = "Declarations\n"
                    + "|-- Name: declaration\n"
                    + "|-- DataType: null\n"
                    + "|-- Name: foo\n"
                    + "|-- DataType: null\n"
                    + "|-- Name: bar\n"
                    + "|-- DataType: null\n";

            // expected vs. actual 
            assertEquals(expectedString, declarationsNode.indentedToString(0));
        } catch (Exception e) {
            // fail if an exception is called 
            fail(e.getMessage());
        }
    }

    /**
     * Program indentedToString return test with manual node creation.
     */
    @Test
    public void testProgram() {
        ProgramNode pNode = new ProgramNode("foo");
        DeclarationsNode dNode = new DeclarationsNode();
        dNode.addVariable(new VariableNode("num"));
        dNode.addVariable(new VariableNode("bar"));
        pNode.setVariables(dNode);
        pNode.setFunctions(new SubProgramDeclarationsNode());
        pNode.setMain(new CompoundStatementNode());

        String test = "program foo; var num, bar : integer; begin end .";
        Parser p = new Parser(test, false);

        String actual = p.program().indentedToString(0);
        String expected = pNode.indentedToString(0);

        assertEquals(expected, actual);
    }

}
