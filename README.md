##Introduction:
This compiler project is for a simplified version of the Pascal language.
Apart from the source code for the project, this project also contains
documentation on the making and using of the compiler as well as a user manual.

##Components:

####/src/compiler/
compilerMain.java
####/src/scanner/
*  LookupTable.java
*  Scanner.java
*  Scanner.jflex
*  TestMyScanner.java
*  Token.Java
*  TokenType.Java
*  jflex-full-1.7.0.jar

####/src/parser/
*  Recognizer.java
*  Parser.java

####/src/symboltable/
*  SymbolTable.java
*  Kind.java

####/src/syntaxtree/
*  AssignmentStatementNode.java
*  CompoundStatementNode.java
*  DeclarationsNode.java
*  ExpressionsNode.java
*  IfStatementNode.java
*  OperationNode.java
*  ProgramNode.java
*  StatementNode.java
*  SubProgramDeclarationsNode.java
*  SubProgramNode.java
*  SyntaxTreeNode.java
*  ValueNode.java
*  VariableNode.java

####/src/analyzer/
*  Analyzer.java

####/src/codegenerator/
*  CodeGenerator.java

####/test/parser/
RecognizerTest.java

####/test/scanner/
ScannerTest.java

####/test/symboltable/
SymbolTableTest.java

####/test/syntaxtree/
SyntaxTreeTest.java

####/src/codegenerator/
*  CodeGeneratorTest.java

##Final Product and Documentation:

####/product/compiler/
*  BautistaCompiler.jar
*  UserManual.pdf
*  test.pas

####/SDD/
*  keywordsAndSymbols.jpg
*  ScannerStateMachine.jpg
*  ScannerUML.jpg
*  SymbolTableUML.jpg
*  SDD.css
*  SDD.html
*  SDD.pdf
