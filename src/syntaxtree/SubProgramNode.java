package syntaxtree;

/**
 * represents a SubProgramNode in a syntax tree.
 *
 * @author Eduardo Flores
 */
public class SubProgramNode extends ProgramNode {

    private SubProgramKind kind;
    private ParametersNode parameters;

    public SubProgramNode(String lexeme) {
        super(lexeme);
        setVariables(new DeclarationsNode());
        setFunctions(new SubProgramDeclarationsNode());
        setMain(new CompoundStatementNode());
    }

    public ParametersNode getParameters() {
        return parameters;
    }

    public SubProgramKind getKind() {
        return kind;
    }

    public void setParameters(ParametersNode parameters) {
        this.parameters = parameters;
    }

    public void setKind(SubProgramKind kind) {
        this.kind = kind;
    }

    /**
     * Creates a String representation of this program node and its children.
     *
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString(int level) {
        String answer = this.indentation(level);
        answer += "SubProgram: " + getName() + "\n";
        answer += getParameters().indentedToString(level + 1);
        answer += getVariables().indentedToString(level + 1);
        answer += getFunctions().indentedToString(level + 1);
        answer += getMain().indentedToString(level + 1);
        return answer;
    }
}
