package syntaxtree;

/**
 * Represents a single While statement.
 *
 * @author Eduardo Flores
 */
public class WhileStatementNode extends StatementNode {
    
    private ExpressionNode conditionExpression;
    private StatementNode doStatement;
    
    public ExpressionNode getTest() {
        return conditionExpression;
    }

    public void setTest(ExpressionNode test) {
        this.conditionExpression = test;
    }

    public StatementNode getDoStatement() {
        return doStatement;
    }

    public void setDoStatement(StatementNode doStatement) {
        this.doStatement = doStatement;
    }
    
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "While\n";
        answer += this.conditionExpression.indentedToString( level + 1);
        answer += this.doStatement.indentedToString( level + 1);
        return answer;
    }
}
