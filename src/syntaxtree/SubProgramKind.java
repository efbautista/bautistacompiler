
package syntaxtree;

/**
 * enum class for the subProgramType
 * @author eddy
 */
public enum SubProgramKind {
    PROCEDURE, FUNCTION
}
