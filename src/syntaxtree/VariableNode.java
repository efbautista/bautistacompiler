package syntaxtree;

/**
 * Represents a variable in the syntax tree.
 *
 * @author Erik Steinmetz
 */
public class VariableNode extends ExpressionNode {

    /**
     * The name of the variable associated with this node.
     */
    String name;
    DataType dataType;

    /**
     * Creates a ValueNode with the given attribute.
     *
     * @param attr The attribute for this value node.
     */
    public VariableNode(String attr) {
        this.name = attr;
        this.dataType = null;
    }

    /**
     * Returns the name of the variable of this node.
     *
     * @return The name of this VariableNode.
     */
    public String getName() {
        return (this.name);
    }

    public void setDataType(DataType type) {
        this.dataType = type;
    }

    /**
     * getter for data type.
     *
     * @return Returns the dataKind of Variable Node.
     *
     */
    public DataType getDataType() {
        return (this.dataType);
    }

    /**
     * Returns the name of the variable as the description of this node.
     *
     * @return The attribute String of this node.
     */
    @Override
    public String toString() {
        return (name);
    }

    /**
     * Creates a String representation of this variable node.
     *
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString(int level) {
        String answer = this.indentation(level);
        answer += "Name: "+this.name+ "Type: "+this.dataKind + "\n";
        return answer;
    }

    @Override
    public boolean equals(Object o) {
        boolean answer = false;
        if (o instanceof VariableNode) {
            VariableNode other = (VariableNode) o;
            if (this.name.equals(other.name)) {
                answer = true;
            }
        }
        return answer;
    }

}
