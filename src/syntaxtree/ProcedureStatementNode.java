package syntaxtree;

import java.util.ArrayList;

/**
 * Node class for procedure statement.
 *
 * @author Eduardo Flores
 */
public class ProcedureStatementNode extends StatementNode {

    private String statementName;
    private ArrayList<ExpressionNode> expressionList = new ArrayList<ExpressionNode>();

    public ProcedureStatementNode(String statementName) {
        this.statementName = statementName;
    }

    public String getName() {
        return statementName;
    }

    /**
     * adds expessionNode to expressionList.
     *
     * @param expressionNode
     */
    public void addExpression(ExpressionNode expressionNode) {
        this.expressionList.add(expressionNode);
    }
    public ArrayList<ExpressionNode> getExpressions(){
        return expressionList;
    }

    @Override
    public String indentedToString(int level) {
        String answer = this.indentation(level);
        answer += "Procedure: " + getName() + "\n";
        for (ExpressionNode expression : expressionList) {
            answer += expression.indentedToString(level + 1);
        }
        return answer;
    }
}
