package syntaxtree;

import java.util.ArrayList;

/**
 * class to keep track of the parameters nodes found in pas file.
 *
 * @author eddy
 */
public class ParametersNode extends SyntaxTreeNode {

    //we will store the paramaters as VariableNodes. 
    private ArrayList<VariableNode> variables = new ArrayList<VariableNode>();

    /**
     * adds a variable to the list of variables, this is a declaration variable.
     *
     * @param var declaration variable to add to variables list.
     */
    public void addVariable(VariableNode var) {
        variables.add(var);
    }

    public ArrayList<VariableNode> getParameter() {
        return variables;
    }

    /**
     * Creates a String representation of this declarations node and its
     * children.
     *
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString(int level) {
        String string = this.indentation(level);
        string += "Parameters\n";
        for (VariableNode variable : variables) {
            string += variable.indentedToString(level + 1);
        }
        return string;
    }
}
