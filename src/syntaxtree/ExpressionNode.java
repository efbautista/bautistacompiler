package syntaxtree;

/**
 * General representation of any expression.
 * @author erik
 */
public abstract class ExpressionNode extends SyntaxTreeNode {
    
	
	public DataType dataKind;
	
	public ExpressionNode() {
		this.dataKind = null;
	}

	public DataType getDataType() {
		return dataKind;
	}

	public void setDataType(DataType dataType) {
		this.dataKind = dataType;
	}
}
