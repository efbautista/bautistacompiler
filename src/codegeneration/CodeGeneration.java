package codegeneration;

import syntaxtree.*;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import scanner.TokenType;

/**
 * a code generation module which uses the syntax tree to produce MIPS assembly
 * language code as its output.
 *
 * @author Eduardo Flores
 */
public class CodeGeneration {

    //the mips register number for f and t registers.
    private static int registerTNumber = 0;
    private static int registerFNumber = 0;

    private static String hex = new String();

    /**
     * writes outline for assembly code and calls writeCode to write the rest of
     * the code.
     *
     * @param filename
     * @param programNode
     */
    public static void assemblyToFile(String filename, ProgramNode programNode) {
        PrintWriter write;
        try {
            write = new PrintWriter(new BufferedWriter(new FileWriter(filename + ".asm")));
            write.println(writeCodeForRoot(programNode));
            write.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * writes asm code for root node into register 0. and calls writeCode
     * function to finish the rest.
     *
     * @param programNode or root node to generate code for.
     * @return String that contains the asm code for root node.
     */
    public static String writeCodeForRoot(ProgramNode programNode) {
        //writing inital 
        String code = ".data\n"
                + "promptuser:    .asciiz \"Enter value: \"" + "\n"
                + "newline:       .asciiz \"\\n\"" + "\n";

        //writing code for declaring variables. 
        if (programNode.getVariables() != null) {
            code += writeCode(programNode.getVariables());
        }
        //writing code for main
        code += "\n.text\nmain:\n";

        if (programNode.getMain() != null) {
            code += writeCode(programNode.getMain());
        }
        //jump to ra register. 
        code += "\njr $ra";
        return (code);
    }

    /**
     * Writes code for DeclarationsNode.
     *
     * @param node
     * @return
     */
    public static String writeCode(DeclarationsNode node) {
        String code = "";
        //creat a list of varaibles found in declarationsNode. 
        ArrayList<VariableNode> variables = node.getVariableNodes();
        for (VariableNode vN : variables) {
            code += String.format("%-10s     .word 0\n", vN.getName() + ":");
        }
        return (code);
    }

    /**
     * assembly code generator for a variable Node.
     *
     * @param variableNode : the variable node to write the code for.
     * @param register : register to write mips code into.
     * @return
     */
    public static String writeCode(VariableNode variableNode, String register) {
        String variableName = variableNode.getName();
        //load into the rlw      esult register, the name of the variable. 
        String code = "lw      " + register + ",   " + variableName + "\n";
        //return back the assembly code for loading a variable into specific register.
        return (code);
    }

    /**
     * assembly code generator for a compound statement.
     *
     * @param csn: the variable node to write the code for.
     * @return String representation of compound statement node for mips.
     */
    public static String writeCode(CompoundStatementNode csn) {
        String code = "";
        ArrayList<StatementNode> statements = csn.getStatements();
        /*go through all the statementNodes in statement and write
         the .asm code for each one*/
        for (StatementNode sn : statements) {
            code += writeCode(sn);
        }
        //return ALL statement nodes forming the compound statement in .asm
        return (code);
    }

    /**
     * finds the type of statement Node given in parameters and returns a
     * assembly string representation of that type.
     *
     * @param csn: the statement node to write the mips code for.
     * @return String containing the mips code for a specific type of
     * statementNode.
     */
    public static String writeCode(StatementNode sn) {
        String code = "";
        /*determin the type of node the statmenetNode is and 
         write the code for that type of node*/
        if (sn instanceof ProcedureStatementNode) {
            code = writeCode((ProcedureStatementNode) sn);
        } else if (sn instanceof AssignmentStatementNode) {
            code = writeCode((AssignmentStatementNode) sn);
        } else if (sn instanceof CompoundStatementNode) {
            code = writeCode((CompoundStatementNode) sn);
        } else if (sn instanceof IfStatementNode) {
            code = writeCode((IfStatementNode) sn);
        } else if (sn instanceof WhileStatementNode) {
            code = writeCode((WhileStatementNode) sn);
        } else if (sn instanceof ReadStatementNode) {
            code = writeCode((ReadStatementNode) sn);
        } else if (sn instanceof WriteStatementNode) {
            code = writeCode((WriteStatementNode) sn);
        }
        return (code);
    }

    /**
     * goes through assignment statement node and generates code as a string
     * representing .asm code.
     *
     * @param asn, the assignment statement node to produce .asm code for.
     * @return String representing .asm code.
     */
    public static String writeCode(AssignmentStatementNode asn) {
        String code = "";
        //we will look into the expressions datatype to compare 
        ExpressionNode expressionNode = asn.getExpression();

        if (expressionNode.getDataType() == DataType.REAL) {
            //in mips it is standard to store into f floating point values.
            String register = "$f" + registerFNumber;
            //write the code 
            code += writeCode(expressionNode, register);
            code += "sw      $f" + registerFNumber + ",   " + asn.getLeftValue() + "\n";
        } else {
            String register = "$t" + registerTNumber;
            //first generate the code for the expression and write it. 
            code += writeCode(expressionNode, register);
            //than store the value of that register into register named the same as left value. 
            code += "sw      $t" + registerTNumber + ",   " + asn.getLeftValue() + "\n";
        }
        return (code);
    }

    /**
     * goes through expression statement node and generates code for specific
     * type of expressionNode as a string representing .asm code.
     *
     * @param expressionNode the expression node to go through.
     * @param register to store the expression into.
     * @return String representing .asm code.
     */
    public static String writeCode(ExpressionNode expressionNode, String register) {
        String nodeCode = "";
        //check to see if its a variable, value or operation node.
        if (expressionNode instanceof VariableNode) {
            nodeCode = writeCode((VariableNode) expressionNode, register);
        } else if (expressionNode instanceof ValueNode) {
            nodeCode = writeCode((ValueNode) expressionNode, register);
        } else if (expressionNode instanceof OperationNode) {
            nodeCode = writeCode((OperationNode) expressionNode, register);
        }
        return (nodeCode);
    }

    /**
     * goes through Value statement node and generates code for specific t
     * string representing .asm code.
     *
     * @param valueNode the node to go through.
     * @param register to store the expression into.
     * @return String representing .asm code.
     */
    public static String writeCode(ValueNode valueNode, String register) {
        String value = valueNode.getAttribute();
        //use add i to instert into the desired register the string of value node. 
        String code = "addi    " + register + ",   $zero, " + value + "\n";
        return (code);
    }

    /**
     * goes through operation node and generates code for specific
     * type of operation as a string representing .asm code.
     *
     * @param operartionNode The node to go through.
     * @param register To store operation into
     * @return string representation of .asm code
     */
    public static String writeCode(OperationNode operationNode, String register) {
        String code = "";
        String firstRegister = "";
        String secondRegister = "";
        //expression node refrences 
        ExpressionNode leftOfOperation = operationNode.getLeft();
        ExpressionNode rightOfOperation = operationNode.getRight();
        //operation type , will be used to determine the type of 
        TokenType operationType = operationNode.getOperation();

        /*before starting code writing determin the data type of expression
         to know which type of register to store in*/
        if (leftOfOperation.getDataType() == DataType.REAL) {
            firstRegister = "$f" + registerFNumber++;//f register for floating in mips
        } else {
            firstRegister = "$t" + registerTNumber++;
            //begin .asm code with the left operationNode, writing for an expression.
            code = writeCode(leftOfOperation, firstRegister);//first reg change
        }

        //again check the left and add to register number
        if (leftOfOperation.getDataType() == DataType.REAL) {
            secondRegister = "$f" + registerFNumber++;
        } else {
            secondRegister = "$t" + registerTNumber++;
        }
        //begin .asm code with the left operationNode, writing for an expression.
        code += writeCode(rightOfOperation, secondRegister);//2nd reg change
        
        /*look through all the code and generate code for the correct token*/
        if (operationType == TokenType.EQUAL) {
            hex = getHex();
            code += "beq     " + secondRegister + ",   " + firstRegister + ",   EqualID" + hex + "\n";
            code += "li      " + register + ",   " + "0\n";
            code += "j       EqualEnd" + hex + "\n";
            code += "Equal" + hex + ":\n";
            code += "li      " + register + ",   " + "1\n";
            code += "EqualEnd" + hex + ":\n";
        } else if (operationType == TokenType.NOTEQUAL) {
            hex = getHex();
            code += "beq     " + secondRegister + ",   " + firstRegister + ",   NotEqualID" + hex + "\n";
            code += "li      " + register + ",   " + "1\n";
            code += "j       NotEqualEnd" + hex + "\n";
            code += "NotEqual" + hex + ":\n";
            code += "li      " + register + ",   " + "0\n";
            code += "NotEqualEnd" + hex + ":\n";
        } else if (operationType == TokenType.LESSTHAN) {
            code += "slt     " + register + ",   " + firstRegister + ",   " + secondRegister + "\n";
        } else if (operationType == TokenType.GREATERTHAN) {
            code += "slt     " + register + ",   " + secondRegister + ",   " + firstRegister + "\n";
        } else if (operationType == TokenType.LESSTHANOREQUALTO) {
            code += "addi    " + secondRegister + ",   " + secondRegister + ",   1\n";
            code += "slt     " + register + ",   " + firstRegister + ",   " + secondRegister + "\n";
        } else if (operationType == TokenType.GREATERTHANOREQUALTO) {
            code += "addi    " + firstRegister + ",   " + firstRegister + ",   1\n";
            code += "slt     " + register + ",   " + secondRegister + ",   " + firstRegister + "\n";
        } else if (operationType == TokenType.PLUS) {
            code += "add     " + register + ",   " + firstRegister + ",   " + secondRegister + "\n";
        } else if (operationType == TokenType.MINUS) {
            code += "sub     " + register + ",   " + firstRegister + ",   " + secondRegister + "\n";
        } else if (operationType == TokenType.SLASH) {
            code += "div     " + firstRegister + ",   " + secondRegister + "\n";
            code += "mflo    " + register + "\n";
        } else if (operationType == TokenType.ASTERISK) {
            code += "mult    " + firstRegister + ",   " + secondRegister + "\n";
            code += "mflo    " + register + "\n";
        }
        //reset register back to zero.
        registerTNumber = 0;
        registerFNumber = 0;
        return (code);
    }
    /**
     * generates a hex value to be used in mips code. 
     * @return String representing generated hex
     */
    private static String getHex() {
        Random random = new Random();
        int val = random.nextInt();
        return Integer.toHexString(val);
    }
    
    /**
     * goes through expression statement node and generates code for specific
     * type of expressionNode as a string representing .asm code.
     *
     * @param ifStatementNode to generate .asm code string for.
     * @return String representation of the ifStatementNode.
     */
    public static String writeCode(IfStatementNode ifStatementNode) {
        String code = "";
        hex = getHex();
        String register = "$t" + registerTNumber;
        code += writeCode(ifStatementNode.getTest(), register);
        //Fail is a mips beq id 
        code += "beq     " + register + ",   $zero, inIfFail" + hex + "\n";
        //Padd is a mips jump id for a pass if statment
        code += writeCode(ifStatementNode.getThenStatement());
        code += "j	inIfPass" + hex + "\n";

        code += "inIfFail" + hex + ":\n";
        //if fail look for else. 
        code += writeCode(ifStatementNode.getElseStatement());
        code += "inIfPass" + hex + ":\n";
        return (code);
    }

    /**
     * goes through whileStatementNode and process WhileStatementNode
     * as a string representing .asm code.
     *
     * @param whileNode while statement node to convert to mips. 
     * @return String representation of the WhileStatementNode.
     */
    public static String writeCode(WhileStatementNode whileNode) {
        String code = "";
        hex = getHex();
        String register = "$t" + registerTNumber;
        code += "inWhile" + hex + ":\n";
        code += writeCode(whileNode.getTest(), register);
        code += "beq     " + register + ",   $zero, whileDone" + hex + "\n";

        code += writeCode(whileNode.getDoStatement());
        code += "j       inWhile" + hex + "\n";

        code += "whileDone" + hex + ":\n";

        return (code);
    }

     /**
     * goes through whileStatementNode and process WhileStatementNode
     * as a string representing .asm code.
     *
     * @param readNode while statement node to convert to mips. 
     * @return String representation of the WhileStatementNode.
     */
    public static String writeCode(ReadStatementNode readNode) {
        String code = "";

        code += "li      $v0,   4\n";
        code += "la      $a0,   prompt\n";
        code += "syscall\n";

        DataType dT = readNode.getName().getDataType();
        if (dT == DataType.INTEGER) {
            code += "li      $v0,   5\n";
            code += "syscall\n";
            code += "sw      $v0,   " + readNode.getName().getName() + "\n";
        } else if (dT == DataType.REAL) {
            code += "li      $v0,   6\n";
            code += "syscall\n";
            code += "sw      $v0,   " + readNode.getName().getName() + "\n";
        }

        return (code);
    }

    /**
    /**
     * goes through WriteStatementNode and determins the instance
     * of write node than generates WriteStatementNode
     * as a string representing .asm code.
     *
     * @param writeNode while statement node to convert to mips. 
     * @return String representation of the writeStatementNode.
     */
    public static String writeCode(WriteStatementNode writeNode) {
        String code = "";
        //first check to see if the write node 
        if (writeNode.getName() instanceof OperationNode) {
            if (writeNode.getName().getDataType() == DataType.INTEGER) {
                code += writeCode(writeNode.getName(), "$s0");
                code += "li      $v0,   1\n";
                code += "move    $a0,   $s0\n";
                code += "syscall\n";
            }
        } else if (writeNode.getName() instanceof VariableNode) {
            code += writeCode(writeNode.getName(), "$s0");
            code += "li      $v0,   1\n";
            code += "move    $a0,   " + writeNode.getName() + "\n";
            code += "syscall\n";
        }
        //new line. 
        code += ""
                + "li      $v0,   4\n"
                + "la      $a0,   newline\n"
                + "syscall\n";

        return code;
    }
    


}//END
