package parser;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import scanner.*;
import symboltable.*;
import syntaxtree.*;
import symboltable.Kind;
import symboltable.SymbolTable;

/**
 * parser class for compiler which matches the token types in the code and
 * generates a program node which represents the pascal file.
 *
 * @author Eduardo Flores
 */
public class Parser {

    ///////////////////////////////
    //    Instance Variables
    ///////////////////////////////
    public Token lookahead;
    public Scanner scanner;
    public SymbolTable symbolTable;

    ///////////////////////////////
    //       Constructor
    ///////////////////////////////
    public Parser(String text, boolean isFilename) {
        symbolTable = new SymbolTable();
        if (isFilename) {
            String filename = System.getProperty("user.dir") + "/" + text + ".pas";
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(filename);
            } catch (FileNotFoundException ex) {
                error("No file");
            }
            InputStreamReader isr = new InputStreamReader(fis);
            scanner = new Scanner(isr);

        } else {
            scanner = new Scanner(new StringReader(text));
        }
        try {
            lookahead = scanner.nextToken();
        } catch (IOException ex) {
            error("Scan error");
        }

    }

    public void match(TokenType expected) {
        System.out.println("match( " + expected + ")");
        if (this.lookahead.getType() == expected) {
            try {
                this.lookahead = scanner.nextToken();
                if (this.lookahead == null) {
                    this.lookahead = new Token("End of File", null);
                }
            } catch (IOException ex) {
                error("Scanner exception");
            }
        } else {
            error("Match of " + expected + " found " + this.lookahead.getType()
                    + " instead.");
        }
    }

    ///////////////////////////////
    //    Production Rules
    ///////////////////////////////
    /**
     * Production Rules: program id ; declarations subprogram_declarations
     * compound_statement period Top of syntax tree is the program node which,
     *
     * matches all tokens in the program Node and creates a program Node.
     *
     * @return ProgramNode which is the root of the .pas file.
     */
    public ProgramNode program() {
        //match program and add it to lexeme
        match(TokenType.PROGRAM);
        ProgramNode pNode = new ProgramNode(lookahead.lexeme);
        this.symbolTable.addKind(lookahead.getLexeme(), Kind.PROGRAM);
        match(TokenType.ID);
        match(TokenType.SEMICOLON);
        //set the main, variables and functions in the program.
        pNode.setVariables(declarations(new DeclarationsNode()));
        pNode.setFunctions(subprogram_declarations(new SubProgramDeclarationsNode()));
        pNode.setMain(compound_statement());
        match(TokenType.PERIOD);
        return pNode;

    }

    /**
     * this function returns the message error when there is an error in our
     * Pascal based language input file.
     *
     * @param message
     */
    public void error(String message) {
        System.out.println("Error: " + message);
    }

    /**
     * Production Rules: var identifier_list:type;declarations | lambda
     *
     * matches tokens for declarations in program, collects all declarations as
     * a listing of variable nodes.
     *
     * @return DeclarationsNode, consisting of all the variables declared.
     */
    public DeclarationsNode declarations(DeclarationsNode dn) {
        ArrayList<VariableNode> variables = new ArrayList<>();
        DataType dataType = null;
        //
        if (lookahead.getType() == TokenType.VAR) {
            match(TokenType.VAR);
            variables = identifier_list(new ArrayList<VariableNode>());
            match(TokenType.COLON);
            dataType = type();
            match(TokenType.SEMICOLON);
            dn = declarations(dn);
        }
        //go through all the variable nodes in variables and set the datatype.
        for (VariableNode variableNode : variables) {
            //set the dataType of that variableNode.
            variableNode.setDataType(dataType);
            /*add the variable with data type to declarations.
             this is needed  for declaration check.*/
            dn.addVariable(variableNode);
            //now add to symbolTable the type for this variable.
            symbolTable.setDataType(variableNode.getName(), dataType);
        }
        return dn;
    }

    /**
     * returns the datatype of based on the lookahead token.
     *
     * @return DataType , an enum for the type of data we can see in our
     * program.
     */
    public DataType type() {
        DataType dataType = null;
        if (lookahead.getType() == TokenType.ARRAY) {
            match(TokenType.ARRAY);
            match(TokenType.LSBRACKET);
            if (lookahead.getType() == TokenType.INTEGER) {
                match(TokenType.INTEGER);
            } else {
                match(TokenType.REAL);
            }
            match(TokenType.COLON);
            if (lookahead.getType() == TokenType.INTEGER) {
                match(TokenType.INTEGER);
            } else {
                match(TokenType.REAL);;
                match(TokenType.RSBRACKET);
                match(TokenType.OF);
            }
        }
        dataType = standard_type();
        return dataType;
    }

    /**
     * production rules: integer | real
     *
     * returns the data type of the declared variable.
     *
     * @return Datatype of the declared variable with error prompt if null.
     */
    public DataType standard_type() {
        if (this.lookahead.type == TokenType.INTEGER) {
            match(TokenType.INTEGER);
            return DataType.INTEGER;
        } else if (this.lookahead.type == TokenType.REAL) {
            match(TokenType.REAL);
            return DataType.REAL;
        } else {
            error("standard_type not valid");
            return null;
            //return a default null datatype.
        }
    }

    /**
     * returns a list of variable nodes.
     *
     * @param variablesList a
     * @return a list of variables, called in declarations and parameter_list.
     */
    public ArrayList<VariableNode> identifier_list(ArrayList<VariableNode> variablesList) {
        variablesList.add(new VariableNode(lookahead.getLexeme()));
        this.symbolTable.addKind(lookahead.getLexeme(), Kind.VARIABLE);
        match(TokenType.ID);
        //if there is a next token.
        if (this.lookahead.type == TokenType.COMMA) {
            match(TokenType.COMMA);
            //call identifier_list with same variable list. 
            variablesList = identifier_list(variablesList);
        }
        return variablesList;
    }

    /**
     * Production Rules: subprogram_declaration; subprogram_declarations | lamda
     *
     * determines the the type of subprogram is in the declaration and adds a
     * created subprogram to its list of subprogram.
     *
     * @return SubProgramDeclarationsNode that is a list of subprogram nodes.
     */
    public SubProgramDeclarationsNode subprogram_declarations(SubProgramDeclarationsNode spdn) {
        //check for procedure and function token type.
        if (this.lookahead.type == TokenType.PROCEDURE
                || this.lookahead.type == TokenType.FUNCTION) {
            //add the correct subprogram declaration type and match correct tokens by calling subprogram_declaration().
            spdn.addSubProgramDeclaration(subprogram_declaration());
            match(TokenType.SEMICOLON);
            //call subprogram to see if there may me another function or procedure declaration.
            spdn = subprogram_declarations(spdn);
        }
        return spdn;
    }

    /**
     * Production Rules: subprogram_head declarations compound_statement returns
     * a suprogram_declaration with set variables, function and main.
     *
     * @return SubProgramNode, a complete sub program node.
     */
    public SubProgramNode subprogram_declaration() {
        //subprogram_head returns a subProgramNode with a declared head.
        SubProgramNode spn = subprogram_head();
        //set all variables by calling declarations and setting a declarationsNode in spn.
        spn.setVariables(declarations(new DeclarationsNode()));
        //set all functions by calling subprogram_declaration  and setting a subProgramDeclarationsNode in spn.
        spn.setFunctions(subprogram_declarations(new SubProgramDeclarationsNode()));
        //set main for the subProgramNode by calling compoundStatement and setting a compoundStatementNode as main in spn.
        spn.setMain(compound_statement());

        return spn;
    }

    /**
     * Production Rules: function id arguments : standard_type ; | procedure id
     * arguments ; sets the kind of SubProgram node is and matches the tokens.
     *
     * determines if the head of the subprogram is for a Function or Procedure
     * and matches tokens accordingly as well as creates the nodes for the
     * subprogram. with declared subprogram heads.
     *
     * @return SubProgramNode subprogram node with its lower nodes set.
     */
    public SubProgramNode subprogram_head() {
        //subprogram return for procedure of function.
        SubProgramNode spn = null;
        //subprogram return and matching for Function Sub Program.
        if (this.lookahead.type == TokenType.FUNCTION) {
            match(TokenType.FUNCTION);
            spn = new SubProgramNode(lookahead.getLexeme());
            spn.setKind(SubProgramKind.FUNCTION);
            this.symbolTable.addKind(lookahead.getLexeme(), Kind.FUNCTION);
            match(TokenType.ID);

            ArrayList<VariableNode> vn = arguments(new ArrayList<VariableNode>());
            ParametersNode pn = new ParametersNode();
            for (VariableNode vN : vn) {
                pn.addVariable(vN);
            }
            spn.setParameters(pn);

            match(TokenType.COLON);
            standard_type();
            match(TokenType.SEMICOLON);
        } //subprogram return and matching for Procedure Sub Program.
        else if (this.lookahead.type == TokenType.PROCEDURE) {
            match(TokenType.PROCEDURE);
            spn = new SubProgramNode(lookahead.getLexeme());
            spn.setKind(SubProgramKind.FUNCTION);
            this.symbolTable.addKind(lookahead.getLexeme(), Kind.PROCEDURE);
            spn.setKind(SubProgramKind.PROCEDURE);
            match(TokenType.ID);

            ArrayList<VariableNode> vNodes = arguments(new ArrayList<VariableNode>());
            ParametersNode pn = new ParametersNode();
            for (VariableNode vn : vNodes) {
                pn.addVariable(vn);
            }
            spn.setParameters(pn);
            match(TokenType.SEMICOLON);
        }
        return spn;
    }

    /**
     * Production Rules: ( parameter_list ) | λ
     *
     * the
     *
     * @param variables which is a list consisting of variable nodes.
     * @return
     */
    public ArrayList<VariableNode> arguments(ArrayList<VariableNode> variables) {
        if (this.lookahead.type == TokenType.LPARENTHESES) {
            match(TokenType.LPARENTHESES);
            //add the variable to the parameter list to avoid duplicates. 
            parameter_list(variables);
            match(TokenType.RPARENTHESES);
        }
        return variables;
    }

    /**
     * Production Rule: identifier_list : type | identifier_list : type ;
     * parameter_list go through all the variables in the paramater and add to
     * the list and match the semicolons .
     *
     * @param vnl, variable node list for a single parameter.
     * @return
     */
    public ArrayList<VariableNode> parameter_list(ArrayList<VariableNode> vnl) {
        ArrayList<VariableNode> vNodes = identifier_list(new ArrayList<VariableNode>());
        match(TokenType.COLON);
        //matches and returns the datatype as an enum. 
        DataType dataType = type();

        if (this.lookahead.type == TokenType.SEMICOLON) {
            match(TokenType.SEMICOLON);
            vnl = parameter_list(vnl);
        }
        //add every variable node in VNode to the the list. 
        for (VariableNode vN : vNodes) {
            vN.setDataType(dataType);
            vnl.add(vN);
            //after setting datatype for node update the symbol table. 
            symbolTable.setDataType(vN.getName(), dataType);
        }
        return vnl;
    }

    /**
     * Production Rules: begin optional_statements end returns a created
     * compound statementNode and Token matches compound statement.
     *
     * returns a new compound statement
     *
     * @return CompoundStatementNode
     */
    public CompoundStatementNode compound_statement() {
        CompoundStatementNode csn = new CompoundStatementNode();
        match(TokenType.BEGIN);
        //with current csn, call for optional statements
        csn = optional_statements(csn);
        match(TokenType.END);
        return csn;
    }

    /**
     * declares statements in compound statement node and returns new compound
     * statement node.
     *
     * @param csn the compound statement to check for.
     * @return a compound statement with declared statements.
     */
    public CompoundStatementNode optional_statements(CompoundStatementNode csn) {
        if (this.lookahead.type == TokenType.ID
                || this.lookahead.type == TokenType.BEGIN
                || this.lookahead.type == TokenType.IF
                || this.lookahead.type == TokenType.WHILE) {

            csn = statement_list(csn);
        }
        return csn;
    }

    /**
     * Production Rules: statement | statement ; statement_list
     *
     * adds statement to compound statement node.
     */
    public CompoundStatementNode statement_list(CompoundStatementNode csn) {
        //add statement to compound statement node created in compound_statement.
        csn.addStatement(statement());
        if (this.lookahead.type == TokenType.SEMICOLON) {
            match(TokenType.SEMICOLON);
            csn = statement_list(csn);
        }
        return csn;
    }

    /**
     * Production Rules. variable assignop expression | procedure_statement |
     * compound_statement | if expression then statement else statement | while
     * expression do statement | read(id) | write ( expression ) return
     * expression
     *
     * returns a complete statement expression as a statementNode and matches
     * tokens accordinly.
     *
     * @return StatementNode, a complete statement.
     */
    public StatementNode statement() {
        //if the next token is begin, return the compound statement node.
        if (this.lookahead.type == TokenType.BEGIN) {
            return compound_statement();
        } //if its an id it can be an assignment or procedure. 
        else if (lookahead.getType() == TokenType.ID) {
            if (this.symbolTable.isKind(lookahead.getLexeme(), Kind.VARIABLE)) {
                AssignmentStatementNode asn = new AssignmentStatementNode();
                asn.setLvalue(variable());
                match(TokenType.BECOMES);
                //set expression by calling expression which returns an expressionNode.
                asn.setExpression(expression());
                return asn;
            } else if (this.symbolTable.isKind(lookahead.getLexeme(), Kind.PROCEDURE)) {
                return procedure_statement();
            } //procedure or variable are our only options so if none than send error. 
            else {
                error("statement().VARorPROC");
            }
        } else if (this.lookahead.type == TokenType.IF) {
            IfStatementNode ifStatementNode = new IfStatementNode();
            match(TokenType.IF);
            ifStatementNode.setTest(expression());
            match(TokenType.THEN);
            ifStatementNode.setThenStatement(statement());
            match(TokenType.ELSE);
            ifStatementNode.setElseStatement(statement());
            return ifStatementNode;
        } else if (this.lookahead.type == TokenType.WHILE) {
            match(TokenType.WHILE);
            expression();
            match(TokenType.DO);
            statement();
            return null; //for now, more nodes to be added.
        } else if (this.lookahead.type == TokenType.READ) {
            match(TokenType.READ);
            match(TokenType.LPARENTHESES);
            match(TokenType.ID);
            match(TokenType.RPARENTHESES);
        } else if (this.lookahead.type == TokenType.WRITE) {
            match(TokenType.WRITE);
            match(TokenType.LPARENTHESES);
            expression();
            match(TokenType.RPARENTHESES);
        } else {
            error("error in statement()");
        }
        return null;
    }

    /**
     * productions Rules: id | id ( expression_list )
     *
     * creates a procedure statement with set expression node and matching for
     * production syntax.
     *
     * @return ProcedureStatementNode that is complete with set expressions.
     */
    public ProcedureStatementNode procedure_statement() {
        ProcedureStatementNode psn = new ProcedureStatementNode(lookahead.lexeme);
        match(TokenType.ID);
        if (this.lookahead.type == TokenType.LPARENTHESES) {
            match(TokenType.LPARENTHESES);
            ArrayList<ExpressionNode> en = expression_list(new ArrayList<ExpressionNode>());
            match(TokenType.RPARENTHESES);

            for (ExpressionNode eN : en) {
                psn.addExpression((eN));
            }
        }
        return psn;
    }

    /**
     * add correct expressions to nodes expression Nodes
     *
     * @param expressionNodes to add expression to.
     * @return a new list where expression Nodes expressions are set.
     */
    public ArrayList<ExpressionNode> expression_list(ArrayList<ExpressionNode> expressionNodes) {
        expressionNodes.add(expression());
        if (lookahead != null && lookahead.getType() == TokenType.COMMA) {
            match(TokenType.COMMA);
            expressionNodes = expression_list(expressionNodes);
        }
        return expressionNodes;
    }

    /**
     * id | id [ expression ]
     */
    public VariableNode variable() {
        String lexeme = lookahead.getLexeme();
        match(TokenType.ID);
        //not worrying about arrays yet.
        if (this.lookahead.type == TokenType.LSBRACKET) {
            ArrayNode an = new ArrayNode(lexeme);
            match(TokenType.LSBRACKET);
            an.setExpression(expression());
            match(TokenType.RSBRACKET);
        }
        return new VariableNode(lexeme);
    }

    /**
     * checks to see if expression of is a Relop expression and matches tokens
     * accordingly , if not the same expression node is returns.
     *
     * @return the new expression node after checking the tokens in expression.
     */
    public ExpressionNode expression() {
        ExpressionNode expressionNode = simple_expression();
        if (isRelop(lookahead)) {
            OperationNode operationNode = new OperationNode(relop());
            operationNode.setLeft(expressionNode);
            operationNode.setRight(simple_expression());
            return operationNode;
        }
        return expressionNode;
    }

    /**
     * term simple_part | sign term simple_part
     */
    public ExpressionNode simple_expression() {
        OperationNode operationNode = null;
        ExpressionNode expressionNode = null;
        if (this.lookahead.type == TokenType.MINUS
                || this.lookahead.type == TokenType.PLUS) {
            operationNode = sign();
        }
        if (operationNode != null) {
            operationNode.setRight(term());
            expressionNode = operationNode;
        } else {
            expressionNode = term();
        }
        expressionNode = simple_part(expressionNode);
        return expressionNode;
    }

    /**
     * creates correct expression node based on the lookahead tokens and returns
     * an expression that has been checked for being a addOp
     *
     * @param expressionNode to examine and update
     * @return updated expression Node.
     */
    public ExpressionNode simple_part(ExpressionNode expressionNode) {
        if (isAddop(lookahead)) {
            OperationNode operationNode = new OperationNode(addop());
            operationNode.setLeft(expressionNode);
            operationNode.setRight(term());
            return simple_part(operationNode);
        }
        return expressionNode;
    }

    /**
     * factor term_part
     */
    public ExpressionNode term() {
        ExpressionNode expressionNode = factor();
        expressionNode = term_part(expressionNode);
        return expressionNode;
    }

    /**
     * mulop factor term_part | λ
     */
    public ExpressionNode term_part(ExpressionNode expressionNode) {
        if (isMulop(lookahead)) {
            OperationNode operationNode = new OperationNode(mulop());
            operationNode.setLeft(expressionNode);
            operationNode.setRight(factor());
            return term_part(operationNode);
        }
        return expressionNode;
    }

    /**
     * creates an expression node according to next token types which can be an
     * array, procedure value or expression. with nodes set according to
     * expression type. Production rules: id | id [ expression ] | id (
     * expression_list ) | num | ( expression ) | not factor
     *
     * @return ExressionNode which is correct to used based on token types
     * scanned.
     */
    public ExpressionNode factor() {
        // id
        if (lookahead.getType() == TokenType.ID) {
            String name = lookahead.getLexeme();
            match(TokenType.ID);
            if (lookahead.getType() == TokenType.LSBRACKET) {
                match(TokenType.LSBRACKET);
                ArrayNode eNode = new ArrayNode(name);
                eNode.setExpression(expression());
                match(TokenType.RSBRACKET);
                return eNode;
            } else if (lookahead.getType() == TokenType.LPARENTHESES) {
                match(TokenType.LPARENTHESES);
                // PROCEDURENODE
                ProcedureNode pNode = new ProcedureNode(name);
                ArrayList<ExpressionNode> eNodes = expression_list(new ArrayList<ExpressionNode>());
                match(TokenType.RPARENTHESES);
                for (ExpressionNode eN : eNodes) {
                    pNode.addExpression(eN);
                }
                return pNode;

            }
            return new VariableNode(name);
        } else if (lookahead.getType() == TokenType.NUMBER ||
                lookahead.getType() == TokenType.REAL ) {
            if (lookahead.getType() == TokenType.NUMBER){
            ValueNode vNode = new ValueNode(lookahead.getLexeme());
            match(TokenType.NUMBER);
            return vNode;
            }else{
                ValueNode vNode = new ValueNode(lookahead.getLexeme());
                 match(TokenType.REAL);
                 return vNode;
            }
        } else if (lookahead.getType() == TokenType.LPARENTHESES) {
            match(TokenType.LPARENTHESES);
            ExpressionNode eNode = expression();
            match(TokenType.RPARENTHESES);
            return eNode;
        } else if (lookahead.getType() == TokenType.NOT) {
            match(TokenType.NOT);
            return factor();
        } else {
            error("factor()");
            return null;
        }
    }

    /**
     * returns an operation node with declared operation and left side. also
     * checks token match while setting correct operation.
     *
     * @return OperationNode with set left value.
     */
    public OperationNode sign() {
        if (this.lookahead.type == TokenType.PLUS) {
            match(TokenType.PLUS);
            OperationNode operationNode = new OperationNode(TokenType.PLUS);
            operationNode.setLeft(operationNode);
            return operationNode;
        } else if (this.lookahead.type == TokenType.MINUS) {
            match(TokenType.MINUS);
            OperationNode operationNode = new OperationNode(TokenType.MINUS);
            operationNode.setLeft(operationNode);
            return operationNode;
        } else {
            //lamda
            return null;
        }
    }

    /**
     * returns the mulop TokenType for lookahead token.
     *
     * @return tokenType of lookahead Token
     */
    public TokenType mulop() {
        if (lookahead.type == TokenType.ASTERISK) {
            match(TokenType.ASTERISK);
            return TokenType.ASTERISK;
        } else if (lookahead.type == TokenType.SLASH) {
            match(TokenType.SLASH);
            return TokenType.SLASH;
        } else if (lookahead.type == TokenType.DIV) {
            match(TokenType.DIV);
            return TokenType.DIV;
        } else if (lookahead.type == TokenType.MOD) {
            match(TokenType.MOD);
            return TokenType.MOD;
        } else if (lookahead.type == TokenType.AND) {
            match(TokenType.AND);
            return TokenType.AND;

        } else {
            return null;
        }

    }

    /**
     * returns the addop TokenType for lookahead token.
     *
     * @return tokenType of lookahead Token
     */
    public TokenType addop() {
        if (lookahead.type == TokenType.PLUS) {
            match(TokenType.PLUS);
            return TokenType.PLUS;
        } else if (lookahead.type == TokenType.MINUS) {
            match(TokenType.MINUS);
            return TokenType.MINUS;
        } else if (lookahead.type == TokenType.OR) {
            match(TokenType.OR);
            return TokenType.OR;
        } else {
            return null;
        }

    }

    /**
     * returns the relop TokenType for lookahead token.
     *
     * @return tokenType of lookahead Token
     */
    public TokenType relop() {
        if (lookahead.type == TokenType.EQUAL) {
            match(TokenType.EQUAL);
            return TokenType.EQUAL;
        } else if (lookahead.type == TokenType.NOTEQUAL) {
            match(TokenType.NOTEQUAL);
            return TokenType.NOTEQUAL;
        } else if (lookahead.type == TokenType.LESSTHAN) {
            match(TokenType.LESSTHAN);
            return TokenType.LESSTHAN;
        } else if (lookahead.type == TokenType.LESSTHANOREQUALTO) {
            match(TokenType.LESSTHANOREQUALTO);
            return TokenType.LESSTHANOREQUALTO;
        } else if (lookahead.type == TokenType.GREATERTHANOREQUALTO) {
            match(TokenType.GREATERTHANOREQUALTO);
            return TokenType.GREATERTHANOREQUALTO;
        } else if (lookahead.type == TokenType.GREATERTHAN) {
            match(TokenType.GREATERTHAN);
            return TokenType.GREATERTHAN;
        } else {
            return null;
        }

    }

    ///////////////////////////////
    //    Lexical Conventions    //
    ///////////////////////////////
    /**
     * returns true if the lookahead token is a addition operator.
     *
     * @param token
     * @return
     */
    public boolean isAddop(Token token) {
        TokenType tokenType = token.type;
        if (tokenType == TokenType.PLUS
                || tokenType == TokenType.MINUS
                || tokenType == TokenType.OR) {

            return true;
        }
        return false;
    }

    /**
     * returns true if lookahead token is a multiplication operator.
     *
     * @param token
     * @return boolean
     */
    public boolean isMulop(Token token) {
        if (this.lookahead.type == TokenType.ASTERISK
                || this.lookahead.type == TokenType.SLASH
                || this.lookahead.type == TokenType.DIV
                || this.lookahead.type == TokenType.MOD
                || this.lookahead.type == TokenType.AND) {

            return true;
        }
        return false;
    }

    /**
     * returns true if the lookahead token is a relational operator.
     *
     * @param token
     * @return boolean
     */
    public boolean isRelop(Token token) {
        if (this.lookahead.type == TokenType.EQUAL
                || this.lookahead.type == TokenType.NOTEQUAL
                || this.lookahead.type == TokenType.LESSTHAN
                || this.lookahead.type == TokenType.GREATERTHAN
                || this.lookahead.type == TokenType.LESSTHANOREQUALTO
                || this.lookahead.type == TokenType.GREATERTHANOREQUALTO) {

            return true;
        }
        return false;
    }
    ///////////////////////////////
    //    other functions
    ///////////////////////////////

    /**
     * saves a filename in directory where it will be ran.
     *
     * @param fileName is the name the file will be named.
     */
    public void writeToFile(String fileName) {
        PrintWriter printWriter;
        try {
            printWriter = new PrintWriter(new BufferedWriter(new FileWriter(fileName + ".symboltable")));
            printWriter.println(this.symbolTable.toString());
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
