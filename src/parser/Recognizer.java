package parser;

import scanner.Scanner;
import scanner.Token;
import scanner.TokenType;
import symboltable.Kind;
import symboltable.SymbolTable;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.BufferedWriter;


/**
 * this Recognize class will identify if a sting of tokens represents a valid
 * program based on rules set for this Pascal based language.
 *
 * @author Eduardo Flores Bautista
 */
public class Recognizer {

    ///////////////////////////////
    //    Instance Variables
    ///////////////////////////////
    public Token lookahead;
    public Scanner scanner;
    public SymbolTable symbolTable;

    ///////////////////////////////
    //       Constructors
    ///////////////////////////////
    public Recognizer(String text, boolean isFilename) {
        symbolTable = new SymbolTable();
        if (isFilename) {
            String filename = System.getProperty("user.dir") + "/" + text + ".pas";
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(filename);
            } catch (FileNotFoundException ex) {
                error("No file");
            }
            InputStreamReader isr = new InputStreamReader(fis);
            scanner = new Scanner(isr);

        } else {
            scanner = new Scanner(new StringReader(text));
        }
        try {
            lookahead = scanner.nextToken();
        } catch (IOException ex) {
            error("Scan error");
        }

    }

    public void match(TokenType expected) {
        System.out.println("match( " + expected + ")");
        if (this.lookahead.getType() == expected) {
            try {
                this.lookahead = scanner.nextToken();
                if (this.lookahead == null) {
                    this.lookahead = new Token("End of File", null);
                }
            } catch (IOException ex) {
                error("Scanner exception");
            }
        } else {
            error("Match of " + expected + " found " + this.lookahead.getType()
                    + " instead.");
        }
    }
    
    

    ///////////////////////////////
    //    Prodcution Rules
    ///////////////////////////////
    /**
     * program is the most basic way to represent the language and when running
     * program.
     */
    public void program() {
        match(TokenType.PROGRAM);
        this.symbolTable.addKind(lookahead.getLexeme(), Kind.PROGRAM);
        match(TokenType.ID);
        match(TokenType.SEMICOLON);
        declarations();
        subprogram_declarations();
        compound_statement();
        match(TokenType.PERIOD);

        PrintWriter write;
        try {
            write = new PrintWriter(new BufferedWriter(new FileWriter(System.getProperty("user.dir") + "/" + "output.symboltable")));
            write.println(this.symbolTable.toString());
            write.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * this function returns the message error when there is an error in our
     * Pascal based language.
     *
     * @param message
     */
    public void error(String message) {
        System.out.println("Error " + message);
    }

    public void declarations() {
        if (lookahead.getType() == TokenType.VAR) {
            match(TokenType.VAR);
            identifier_list();
            match(TokenType.COLON);
            type();
            match(TokenType.SEMICOLON);
            declarations();
        } else {
            //lambda option.
        }
    }

    /**
     * the type of a variable is an array if not call to standard type function.
     */
    public void type() {
        if (lookahead.getType() == TokenType.ARRAY) {
            match(TokenType.ARRAY);
            match(TokenType.LSBRACKET);
            match(TokenType.NUMBER);
            match(TokenType.COLON);
            match(TokenType.NUMBER);
            match(TokenType.RSBRACKET);
            match(TokenType.OF);
        }
        standard_type();
    }

    /**
     *
     */
    public void standard_type() {
        if (this.lookahead.type == TokenType.INTEGER) {
            match(TokenType.INTEGER);
        } else if (this.lookahead.type == TokenType.REAL) {
            match(TokenType.REAL);
        } else {
            error("error");
        }
    }

    public void identifier_list() {
        this.symbolTable.addKind(lookahead.getLexeme(), Kind.VARIABLE);
        match(TokenType.ID);
        if (this.lookahead.type == TokenType.COMMA) {
            match(TokenType.COMMA);
            identifier_list();
        } else {
            //lamda option
        }
    }

    /**
     * determines if a procedure or function is a valid subprogram declaration.
     */
    public void subprogram_declarations() {
        if (this.lookahead.type == TokenType.PROCEDURE
                || this.lookahead.type == TokenType.FUNCTION) {
            subprogram_declaration();
            match(TokenType.SEMICOLON);
            subprogram_declarations();
        }
    }

    public void subprogram_declaration() {
        subprogram_head();
        declarations();
        compound_statement();
    }

    public void subprogram_head() {
        if (this.lookahead.type == TokenType.FUNCTION) {
            match(TokenType.FUNCTION);
            this.symbolTable.addKind(lookahead.getLexeme(), Kind.FUNCTION);
            match(TokenType.ID);
            arguments();
            match(TokenType.COLON);
            standard_type();
            match(TokenType.SEMICOLON);
        } else if (this.lookahead.type == TokenType.PROCEDURE) {
            match(TokenType.PROCEDURE);
            this.symbolTable.addKind(lookahead.getLexeme(), Kind.PROCEDURE);
            match(TokenType.ID);
            arguments();
            match(TokenType.SEMICOLON);
        }
    }

    public void arguments() {
        if (this.lookahead.type == TokenType.LPARENTHESES) {
            match(TokenType.LPARENTHESES);
            parameter_list();
            match(TokenType.RPARENTHESES);
        } else {
            //lambda
        }
    }

    public void parameter_list() {
        identifier_list();
        match(TokenType.COLON);
        type();
        if (this.lookahead.type == TokenType.SEMICOLON) {
            match(TokenType.COLON);
            parameter_list();
        } else {
            //lambda
        }
    }

    public void compound_statement() {
        match(TokenType.BEGIN);
        optional_statments();
        match(TokenType.END);
    }
    /*not to sure about this, double check*/

    public void optional_statments() {
        if (this.lookahead.type == TokenType.ID
                || this.lookahead.type == TokenType.BEGIN
                || this.lookahead.type == TokenType.IF
                || this.lookahead.type == TokenType.WHILE) {
            statement_list();
        } else {
            //lambda 
        }
    }

    public void statement_list() {
        statement();
        if (this.lookahead.type == TokenType.SEMICOLON) {
            statement_list();
        }
    }

    public void statement() {
        if (lookahead != null && lookahead.getType() == TokenType.ID && 
            this.symbolTable.isKind(lookahead.getLexeme(), Kind.VARIABLE)){
            variable();
            match(TokenType.BECOMES);
            expression();
        } else if (lookahead != null && lookahead.getType() == TokenType.ID
                && this.symbolTable.isKind(lookahead.getLexeme(), Kind.PROCEDURE)) {
            procedure_statement();
        } else if (this.lookahead.type == TokenType.BEGIN) {
            compound_statement();
        } else if (this.lookahead.type == TokenType.IF) {
            match(TokenType.IF);
            expression();
            match(TokenType.THEN);
            statement();
            match(TokenType.ELSE);
            statement();
        } else if (this.lookahead.type == TokenType.WHILE) {
            match(TokenType.WHILE);
            expression();
            match(TokenType.DO);
            statement();
        } else if (this.lookahead.type == TokenType.READ) {
            match(TokenType.READ);
            match(TokenType.LPARENTHESES);
            match(TokenType.ID);
            match(TokenType.RPARENTHESES);
        } else if (this.lookahead.type == TokenType.WRITE) {
            match(TokenType.WRITE);
            match(TokenType.LPARENTHESES);
            expression();
            match(TokenType.RPARENTHESES);
        } else if (this.lookahead.type == TokenType.RETURN) {
            expression();
        }
    }

    public void procedure_statement() {
        match(TokenType.ID);
        if (this.lookahead.type == TokenType.WRITE) {
            match(TokenType.LPARENTHESES);
            expression_list();
            match(TokenType.RPARENTHESES);
        }
    }

    public void expression_list() {
        if (this.lookahead.type == TokenType.ID
                || this.lookahead.type == TokenType.NUMBER
                || this.lookahead.type == TokenType.LPARENTHESES
                || this.lookahead.type == TokenType.NOT
                || this.lookahead.type == TokenType.PLUS
                || this.lookahead.type == TokenType.MINUS) {

            expression();

            if (this.lookahead.type == TokenType.COMMA) {
                match(TokenType.COMMA);
                expression_list();
            } else {
                //lambda
            }
        }
    }

    public void variable() {
        match(TokenType.ID);
        if (this.lookahead.type == TokenType.LSBRACKET) {
            match(TokenType.LSBRACKET);
            expression();
            match(TokenType.RSBRACKET);
        }
    }

    public void expression() {
        if (this.lookahead.type == TokenType.ID
                || this.lookahead.type == TokenType.NUMBER
                || this.lookahead.type == TokenType.LPARENTHESES
                || this.lookahead.type == TokenType.NOT
                || this.lookahead.type == TokenType.PLUS
                || this.lookahead.type == TokenType.MINUS) {

            simple_expression();

            if (isRelop(this.lookahead)) {
                match(TokenType.EQUAL);
                match(TokenType.NOTEQUAL);
                match(TokenType.LESSTHAN);
                match(TokenType.GREATERTHAN);
                match(TokenType.LESSTHANOREQUALTO);
                match(TokenType.GREATERTHANOREQUALTO);

                simple_expression();
            }

        }
    }

    public void simple_expression() {
        if (this.lookahead.type == TokenType.ID
                || this.lookahead.type == TokenType.NUMBER
                || this.lookahead.type == TokenType.LPARENTHESES
                || this.lookahead.type == TokenType.NOT) {

            term();
            simple_part();
        } else if (this.lookahead.type == TokenType.PLUS
                || this.lookahead.type == TokenType.PLUS) {

            sign();
            term();
            simple_part();
        }
    }

    public void simple_part() {
        if (isAddop(lookahead)) {
            match(TokenType.PLUS);
            match(TokenType.MINUS);
            match(TokenType.OR);

            term();
            simple_part();
        }
    }

    public void term() {
        factor();
        term_part();
    }

    public void term_part() {
        if (isMulop(lookahead)) {
            match(TokenType.ASTERISK);
            match(TokenType.SLASH);
            match(TokenType.DIV);
            match(TokenType.MOD);

            factor();
            term_part();
        }
    }

    public void factor() {
        //ID
        if (this.lookahead.type == TokenType.ID) {
            match(TokenType.ID);
            if (this.lookahead.type == TokenType.LSBRACKET) {
                match(TokenType.LSBRACKET);
                expression();
                match(TokenType.RSBRACKET);
            } else if (this.lookahead.type == TokenType.LPARENTHESES) {
                match(TokenType.LPARENTHESES);
                expression_list();
                match(TokenType.RPARENTHESES);
            }
        } else if (this.lookahead.type == TokenType.NUMBER) {
            match(TokenType.NUMBER);

        } else if (this.lookahead.type == TokenType.LPARENTHESES) {
            match(TokenType.LPARENTHESES);
            expression();
            match(TokenType.RPARENTHESES);

        } else if (this.lookahead.type == TokenType.LPARENTHESES) {
            match(TokenType.NOT);
            factor();
        } else {
            //lamda
        }
    }

    public void sign() {
        if (this.lookahead.type == TokenType.PLUS) {
            match(TokenType.PLUS);
        } else if (this.lookahead.type == TokenType.MINUS) {
            match(TokenType.MINUS);

        } else {
            //lamda
        }
    }

    ///////////////////////////////
    //    Lexical Conventions    //  
    ///////////////////////////////
    /**
     * returns true if the lookahead token is a addition operator.
     *
     * @param token
     * @return
     */
    public boolean isAddop(Token token) {
        TokenType tokenType = token.type;
        if (tokenType == TokenType.PLUS
                || tokenType == TokenType.MINUS
                || tokenType == TokenType.OR) {

            return true;
        }
        return false;
    }

    /**
     * returns true if lookahead token is a multiplication operator.
     *
     * @param token
     * @return boolean
     */
    public boolean isMulop(Token token) {
        if (this.lookahead.type == TokenType.ASTERISK
                || this.lookahead.type == TokenType.SLASH
                || this.lookahead.type == TokenType.DIV
                || this.lookahead.type == TokenType.MOD
                || this.lookahead.type == TokenType.AND) {

            return true;
        }
        return false;
    }

    /**
     * returns true if the lookahead token is a relational operator.
     *
     * @param token
     * @return boolean
     */
    public boolean isRelop(Token token) {
        if (this.lookahead.type == TokenType.EQUAL
                || this.lookahead.type == TokenType.NOTEQUAL
                || this.lookahead.type == TokenType.LESSTHAN
                || this.lookahead.type == TokenType.GREATERTHAN
                || this.lookahead.type == TokenType.LESSTHANOREQUALTO
                || this.lookahead.type == TokenType.GREATERTHANOREQUALTO) {

            return true;
        }
        return false;
    }
    ///////////////////////////////
    //    other functions 
    ///////////////////////////////
    
    /**
     * saves a filename in directory where it will be ran.
     * @param fileName is the name the file will be named.
     */
    public void writeToFile(String fileName){
        PrintWriter printWriter;
        try{
            printWriter = new PrintWriter(new BufferedWriter (new FileWriter(System.getProperty("user.dir")+ "/" + fileName + "symbolTable")));
            printWriter.println(this.symbolTable.toString());
            printWriter.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    
}
