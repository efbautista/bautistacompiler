package analyzer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import scanner.TokenType;
import syntaxtree.*;

/**
 * The Analyzer traverses through all nodes in a program to catch any errors
 * that may result in non executable code.
 *
 * @author Eduardo Flores
 */
public class Analyzer {

    private ProgramNode programNode;
    //true if error Exist in code. 
    private boolean codeError;

    /**
     * the constructor class sets the top most node (program node) and sets the
     * codeError to be false.
     *
     * @param programNode, the top most node, needed to traverse tree.
     */
    public Analyzer(ProgramNode programNode) {
        this.programNode = programNode;
        this.codeError = false;
    }

    /*SETTERS AND GETTERS*/
    public boolean getCodeError() {
        return codeError;
    }

    public void setFlag(boolean flag) {
        this.codeError = flag;
    }

    public ProgramNode getPNode() {
        return programNode;
    }

    /**
     * This method calls "analyzeVariables()" which is a method that
     * checks to see if all the variables that are used were declared. The next
     * method it calls is "checkVariablesTypes()" which is a method that checks
     * to see if all the variables have the correct types.
     */
    public void Analyze() {
        this.analyzeVariables();
        //this.checkVariablesTypes();
    }

    /**
     * Method that checks the variable types that are used in the code. If there
     * is a real assigned to an integer variable, the code is flagged.
     */
    private void checkVariablesTypes() {
        for (StatementNode sN : programNode.getMain().getStatements()) {
            analyzeStatementNode(sN);
        }
    }

    /**
     * analyzes StatmentNode by going through statement Node recursively. 
     * @param statementNode, which is the statementNode which is being analyzed.
     * @return 
     */
    private DataType analyzeStatementNode(StatementNode statementNode) {
        //check to see if statmenent contains an assignment statement node. 
        if (statementNode instanceof AssignmentStatementNode) {
            //go thorugh left value of expression first, to traverse through statements expressionNode. 
            analyzeExpressionType(((AssignmentStatementNode) statementNode).getLeftValue());
            //get the expression and datatpe of the left value 
            DataType leftValueDataType = ((AssignmentStatementNode) statementNode).getLeftValue().getDataType();
            //get the datatpe of expression in statement. 
            DataType expressionDataType = analyzeExpressionType(((AssignmentStatementNode) statementNode).getExpression());

            if (leftValueDataType != expressionDataType) {
                //analyze left and right values of assignment statement for REAL datatypes. 
                if (leftValueDataType == DataType.REAL || expressionDataType == DataType.REAL) {
                    //first check to see if the left value contains the REAL datatype. 
                    if (leftValueDataType == DataType.REAL) {
                        ((AssignmentStatementNode) statementNode).getExpression().setDataType(DataType.REAL);
                    } 
                    //if only the expression contains the datatype. 
                    else if (expressionDataType == DataType.REAL) {
                        System.out.println("variable: " + ((AssignmentStatementNode) statementNode).getLeftValue().getName()
                                + "is assigned REAL without declaration");
                        ((AssignmentStatementNode) statementNode).getLeftValue().setDataType(DataType.REAL);
                        this.codeError = true;
                    }
                } 
                //repeat the process for INTEGER datatype. 
                else if (leftValueDataType == DataType.INTEGER || expressionDataType == DataType.INTEGER) {
                    //if the left side contains the integer 
                    if (leftValueDataType == DataType.INTEGER) {
                        System.out.println("variable : " + ((AssignmentStatementNode) statementNode).getLeftValue().getName()
                                + "is assigned to expression with unknown datatype.");
                        ((AssignmentStatementNode) statementNode).getExpression().setDataType(DataType.INTEGER);
                        this.codeError = true;
                    }//no need to check the right values since they wont contain a varaible type. 
                }
            }
        }
        //analyze If statement Nodes. 
        else if (statementNode instanceof IfStatementNode) {
            analyzeExpressionType(((IfStatementNode) statementNode).getTest());
            analyzeStatementNode(((IfStatementNode) statementNode).getThenStatement());
            analyzeStatementNode(((IfStatementNode) statementNode).getElseStatement());
        } 
        //analyze while statement Nodes. 
        else if (statementNode instanceof WhileStatementNode) {
            analyzeExpressionType(((WhileStatementNode) statementNode).getTest());
            analyzeStatementNode(((WhileStatementNode) statementNode).getDoStatement());
        }
        //analyze while Procedure statement Nodes. 
        else if (statementNode instanceof ProcedureStatementNode) {
            for (ExpressionNode eN : ((ProcedureStatementNode) statementNode).getExpressions()) {
                analyzeExpressionType(eN);
            }
        } 
        //analyze while read statement Nodes. 
        else if (statementNode instanceof ReadStatementNode) {
         DataType dT = analyzeExpressionType(((ReadStatementNode) statementNode).getName());
         ((ReadStatementNode) statementNode).getName().setDataType(dT);
         } 
        //analyze write Procedure statement Nodes. 
        else if (statementNode instanceof WriteStatementNode) {
         DataType dT = analyzeExpressionType(((WriteStatementNode) statementNode).getName());
         ((WriteStatementNode) statementNode).getName().setDataType(dT);
         } 
        //analyze while compound statement Nodes. 
        else if (statementNode instanceof CompoundStatementNode) {
            for (StatementNode sN : ((CompoundStatementNode) statementNode).getStatements()) {
                analyzeStatementNode(sN);
            }
        }
        return null;
    }

    /**
     * analyzes the an expression node by recursively running through expression 
     * node and checking all instances of expression node. 
     *
     * @param expressionNode
     * @return
     */
    private DataType analyzeExpressionType(ExpressionNode expressionNode) {
        //if the expression Node contains a value Node. 
        if (expressionNode instanceof ValueNode) {
            //do Nothing for now. 
        } else if (expressionNode instanceof VariableNode) {
            //if an instance of variableNode check for null in datatype
            /*if (((VariableNode) expressionNode).getDataType() == null){
                codeError = true;
            }*/
        } else if (expressionNode instanceof OperationNode) {
            //get the operarionsNode left and right datatypes to analyze. 
            DataType opLeftDataType = analyzeExpressionType(((OperationNode) expressionNode).getLeft());
            DataType opRightDataType = analyzeExpressionType(((OperationNode) expressionNode).getRight());
            /*get the operrationNode's token type
            and check to see if it is of type: boolean, real, or integer.*/
            TokenType operationTokenType = ((OperationNode) expressionNode).getOperation();
            if (operationTokenType == TokenType.EQUAL || 
                    operationTokenType == TokenType.NOTEQUAL || 
                    operationTokenType == TokenType.GREATERTHAN || 
                    operationTokenType == TokenType.GREATERTHANOREQUALTO || 
                    operationTokenType == TokenType.LESSTHAN ||
                    operationTokenType == TokenType.LESSTHANOREQUALTO) {
                expressionNode.setDataType(DataType.BOOLEAN);
                return DataType.BOOLEAN;
            }
            //check the left and right operatior Node and return its datatpe. 
            if (opLeftDataType == DataType.INTEGER || opRightDataType == DataType.INTEGER) {
                expressionNode.setDataType(DataType.INTEGER);
                return DataType.INTEGER;
            } else if (opLeftDataType == DataType.REAL || opRightDataType == DataType.REAL) {
                expressionNode.setDataType(DataType.REAL);
                return DataType.REAL;
            }

        } else if (expressionNode instanceof ProcedureNode) {
            
            //do nothing for now. COME BACK TO LATER!
        }
        return expressionNode.getDataType();
    }

    /**
     * Method that checks the variable that are used in the code. If
     * there are any variables that aren't declared but used, the code is
     * flagged.
     */
    public void analyzeVariables() {
        ArrayList<VariableNode> variablesDeclared = new ArrayList<VariableNode>();
        ArrayList<VariableNode> variablesUsed = new ArrayList<VariableNode>();
        variablesDeclared.addAll(programNode.getVariables().getVariableNodes());

        if (this.programNode instanceof SubProgramNode) {
            variablesDeclared.addAll((((SubProgramNode) programNode).getParameters()).getParameter());
        }
        for (StatementNode sn : programNode.getMain().getStatements()) {
            variablesUsed.addAll(analyzeStatement(sn));
        }
        //go through all the ised variables and see if they are in declared list. 
        for (VariableNode vn : variablesUsed) {
            if (!variablesDeclared.contains(vn)) {
                System.out.println("variable :" + vn.getName() + " is undeclared.");
                this.codeError = true;
            }
        }
        for (SubProgramNode pN : programNode.getFunctions().getProcs()) {
            Analyzer sA = new Analyzer(pN);
            sA.analyzeVariables();
        }
    }

    /**
     * analyzes the statements in a program by going through all statements nodes
     * which include: Assignment, If, While, Procedure, Read and write. 
     *
     * @param statementNode
     */
    private static ArrayList<VariableNode> analyzeStatement(StatementNode statementNode) {
        ArrayList<VariableNode> vNodes = new ArrayList<VariableNode>();

        if (statementNode instanceof AssignmentStatementNode) {
            vNodes.add(((AssignmentStatementNode) statementNode).getLeftValue());
            vNodes.addAll(analyzeExpression(((AssignmentStatementNode) statementNode).getExpression()));
        } else if (statementNode instanceof IfStatementNode) {
            vNodes.addAll(analyzeExpression(((IfStatementNode) statementNode).getTest()));
            vNodes.addAll(analyzeStatement(((IfStatementNode) statementNode).getThenStatement()));
            vNodes.addAll(analyzeStatement(((IfStatementNode) statementNode).getElseStatement()));
        } else if (statementNode instanceof WhileStatementNode) {
            vNodes.addAll(analyzeExpression(((WhileStatementNode) statementNode).getTest()));
            vNodes.addAll(analyzeStatement(((WhileStatementNode) statementNode).getDoStatement()));
        } else if (statementNode instanceof ProcedureStatementNode) {
            for (ExpressionNode eN : ((ProcedureStatementNode) statementNode).getExpressions()) {
                vNodes.addAll(analyzeExpression(eN));
            }
        }else if (statementNode instanceof ReadStatementNode) {
         vNodes.addAll(analyzeExpression(((ReadStatementNode) statementNode).getName()));
         } else if (statementNode instanceof WriteStatementNode) {
         vNodes.addAll(analyzeExpression(((WriteStatementNode) statementNode).getName()));
         }
        return vNodes;
    }
    /**
     * analyzes the expressions in a program by going through all expression
     * nodes which include: value, variable, operation and procedure nodes. 
     * Recursive method that traverses through all expression nodes.
     *
     * @param en,  the expression node to analyze. 
     */
    private static ArrayList<VariableNode> analyzeExpression(ExpressionNode en) {
        ArrayList<VariableNode> vNodes = new ArrayList<VariableNode>();
        if (en instanceof ValueNode) {
        } else if (en instanceof VariableNode) {
            vNodes.add((VariableNode) en);
        } else if (en instanceof OperationNode) {
            vNodes.addAll(analyzeExpression(((OperationNode) en).getLeft()));
            vNodes.addAll(analyzeExpression(((OperationNode) en).getRight()));
        } else if (en instanceof ProcedureNode) {
            for (ExpressionNode eN : ((ProcedureNode) en).getExpressions()) {
                vNodes.addAll(analyzeExpression(eN));
            }
        }
        return vNodes;
    }
    /**
     * writes the indented to string of program node to file. 
     * @param filename, name of what to name the file that contains the indented
     * to string of the programNode. 
     */
    public void writeCodeToFile(String fileName) {
        PrintWriter printWriter;
        try {
            printWriter = new PrintWriter(new BufferedWriter(new FileWriter(fileName + ".syntaxtree")));
            printWriter.println(programNode.indentedToString(0));
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
