package compiler;

import parser.*;
import syntaxtree.*;
import analyzer.*;
import codegeneration.*;

/**
 * this compiler class 
 *
 * @author Eduardo Flores
 */
public class CompilerMain {

    public static void main(String[] args) {
        //if there are no arguments 
        if(args.length < 1){
            System.out.println("no argument for .pas filename");
            System.exit(1);
        }
    
        //currently running test.txt file in root directory.
        //avoids cmd error parser package not available.
        String filename = args[0];//"test";

        //Recognizer recognizer = new Recognizer(filename, true);
        Parser parser;
        try {
            parser = new Parser(filename, true);
            ProgramNode programNode = parser.program();
            // ProgramNode pNode = parser.program();
            Analyzer a = new Analyzer(programNode);

            parser.writeToFile(filename);
            a.Analyze();
            //  System.out.println(pNode.indentedToString(0));

            if (a.getCodeError() == true) {
                System.out.println("code generated a flag");
            } else {
                CodeGeneration.assemblyToFile(filename, programNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
