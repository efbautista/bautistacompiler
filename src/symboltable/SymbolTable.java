package symboltable;

import scanner.Token;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import syntaxtree.DataType;

/**
 * stores identifier names and the type with hash-map to invoke into our
 * recognizer.
 *
 * @author Eduardo Flores
 */
public class SymbolTable {
    /*
     Hashmap consistst of a String
     and Type. Kind will contain a
     Lexeme and Kind when added to
     symbol table.
     */

    private HashMap<String, LexemeData> symbolTable;

    /**
     * constructor.
     */
    public SymbolTable() {
        symbolTable = new HashMap<String, LexemeData>();
    }

    ///////////////////////////////
    //   Data Kind Functions
    ///////////////////////////////
    /**
     * adds the lexeme and kind into lookup table.
     *
     * @param lexeme
     * @param kind
     */
    public void addKind(String lexeme, Kind kind) {
        this.symbolTable.put(lexeme, new LexemeData(lexeme, kind));
    }

    /**
     *
     * @param lexeme
     * @return
     */
    public Kind getKind(String lexeme) {
        if (this.symbolTable.containsKey(lexeme)) {
            return this.symbolTable.get(lexeme).getKind();
        }
        return null;
    }

    /**
     * determines if lexeme and kind match a variable of both types.
     *
     * @param lexeme
     * @param kind
     * @return true if lexeme and kind match a variable of both types.
     */
    public boolean isKind(String lexeme, Kind kind) {
        if (this.getKind(lexeme) == kind) {
            return true;
        }
        return false;
    }
        ///////////////////////////////
    //   Data Type Functions
    ///////////////////////////////

    public DataType getDataType(String lexeme) {
        if (this.symbolTable.containsKey(lexeme)) {
            return this.symbolTable.get(lexeme).getDataType();
        }
        return null;
    }

    /**
     * Returns the datatype using lexeme as key value.
     *
     * @param lexeme
     * @return
     */
    public void setDataType(String lexeme, DataType dataType) {
        if (this.symbolTable.containsKey(lexeme)) {
            this.symbolTable.get(lexeme).setDataType(dataType);
        }
    }

    /**
     * overriding to string to be able to print out symbols to file.
     */
    @Override
    public String toString() {
        String done = "\n**************DONE***************\n";
        for (Entry<String, LexemeData> entry : this.symbolTable.entrySet()) {
            done = "KeyEntry: " + entry.getKey() + "\tKind: " + entry.getValue().getKind() +"\tDataType:" +entry.getValue().getDataType()+"\n" + done;

        }
        return done;
    }

    ///////////////////////////////
    //  lexeme Data
    ///////////////////////////////
    private class LexemeData {

        private String lexeme;
        private Kind kind;
        private DataType dataType;

        /**
         * constructor class for Symbols that are added into symbol table.
         *
         * @param lexeme
         * @param kind
         */
        private LexemeData(String lexeme, Kind kind) {
            this.lexeme = lexeme;
            this.kind = kind;

        }

        private void setDataType(DataType lexeme) {
            this.dataType = lexeme;
        }

        /**
         * returns identifier Type in symbol table.
         *
         * @return Type
         */
        private DataType getDataType() {
            return dataType;
        }

        /**
         * returns kind of identifier for usage in symbol table.
         *
         * @return kind
         */
        private Kind getKind() {
            return this.kind;
        }
    }

}
