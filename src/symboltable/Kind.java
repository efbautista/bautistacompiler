package symboltable;

/**
 *this class is used to identify 
 * and categorizing different types of 
 * identifiers in the symbol table.
 * @author eddy
 */
public enum Kind {
    PROGRAM, VARIABLE, ARRAY, FUNCTION, PROCEDURE,
    
}
