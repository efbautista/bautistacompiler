package scanner;
/**
 * This is a simple example of a jflex lexer definition
 * that is not a standalone application
 * @ author Eduardo Flores
 */

/* Declarations */


%%
%public
%class     Scanner   /* Names the produced java file */
%function  nextToken /* Renames the yylex() function */
%type      Token      /* Defines the return type of the scanning function */


%eofval{
    return null;
%eofval}

%{
    /*this will create our lookupTable.*/
    LookupTable lookup = new LookupTable();
%}

/* Patterns */
letter        = [A-Za-z]

whitespace    = [ \n\t\r]
digit         = [0-9]

id            = {letter}({letter}|{digit}|"_")*
E             = [E]
sign          = [\+\-]

integer       = {digit}{digit}*
decimal       = {integer}(\.){integer}*
exponent      = ({integer}|{decimal}){E}{sign}{integer}

number        = {integer} | {decimal} | {exponent}
real          = {decimal}|{exponent}

symbol        =[.|,|.|:|;|(|)|\[|\]|<|>|*|/|+|\-|=]|<>|:=|<=|>=
other         = .

%%

/* Lexical Rules */

{whitespace}  {  /* Ignore Whitespace */

              }
{id}       {
            /*Finding ID*/

            if (lookup.get(yytext()) != null) return new Token(yytext(), lookup.get(yytext()));
            return new Token(yytext(), TokenType.ID);
           }

{number}   {
            /*Finding Number*/
            return new Token(yytext(), TokenType.NUMBER);
            }

{real}      {
            /*Finding Real*/
            return new Token(yytext(), TokenType.REAL);
            }

{symbol}    {
            /*finding Symbol*/
            return new Token(yytext(), lookup.get(yytext()));
            }
{other}     {
            /*finding Other*/
            return new Token(yytext(), TokenType.ILLEGAL);
            }
