package scanner;

/**
 * A simple Token class containing only a String.
 */
public class Token {

    public String lexeme;
    public TokenType type;

    /**
     * Token constructor
     *
     * @param l is the lexeme read by the scanner.
     * @param t is the TokenType to be connected with string in look up table.
     */
    public Token(String l, TokenType t) {
        this.lexeme = l;
        this.type = t;
    }

    public String getLexeme() {
        return this.lexeme;
    }

    public TokenType getType() {
        return this.type;

    }

    public void setType(TokenType t) {
        this.type = t;
    }

    public void setLexeme(String s) {
        this.lexeme = s;
    }

    /**
     * overriding the hex-codes used by default to string will return lexeme and
     * token type of a token.
     *
     * @return String
     */
    @Override
    public String toString() {
        return ("Token Type: " + type + " Lexeme: " + lexeme);
    }
}
