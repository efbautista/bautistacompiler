package scanner;
/**
 *@author Eduardo Floress
 *look up table for non standalone jflex scanner
 */
import java.util.HashMap;
public class LookupTable extends HashMap <String, TokenType> {
    /**
      *constructor method
      *each lexeme has a token type.
      */
    public LookupTable(){
    HashMap<String, TokenType> Map = new HashMap<String, TokenType>();
                //symbols
   	 	this.put(";", TokenType.SEMICOLON);
  		this.put(",", TokenType.COMMA);
  		this.put(".", TokenType.PERIOD);
  		this.put("-", TokenType.MINUS);
  		this.put("+", TokenType.PLUS);
  		this.put("=", TokenType.EQUAL);
  		this.put("/", TokenType.SLASH);
  		this.put("*", TokenType.ASTERISK);
  		this.put(":", TokenType.COLON);
  		this.put("[", TokenType.LSBRACKET);
  		this.put("]", TokenType.RSBRACKET);
  		this.put("(", TokenType.LPARENTHESES);
  		this.put(")", TokenType.RPARENTHESES);
  		this.put("<>", TokenType.NOTEQUAL);
  		this.put("<", TokenType.LESSTHAN);
  		this.put("<=", TokenType.LESSTHANOREQUALTO);
  		this.put(">", TokenType.GREATERTHAN);
  		this.put(">=", TokenType.GREATERTHANOREQUALTO);
  		this.put(":=", TokenType.BECOMES);
                //keywords
  		this.put("write", TokenType.WRITE);
  		this.put("read", TokenType.READ);
   	 	this.put("and", TokenType.AND);
   	 	this.put("if", TokenType.IF);
   	 	this.put("else", TokenType.ELSE);
  		this.put("array", TokenType.ARRAY);
  		this.put("do", TokenType.DO);
  		this.put("end", TokenType.END);
  		this.put("div", TokenType.DIV);
  		this.put("of", TokenType.OF);
  		this.put("mod", TokenType.MOD);
  		this.put("not", TokenType.NOT);
  		this.put("or", TokenType.OR);
  		this.put("procedure", TokenType.PROCEDURE);
  		this.put("function", TokenType.FUNCTION);
  		this.put("program", TokenType.PROGRAM);
  		this.put("integer", TokenType.INTEGER);
  		this.put("var", TokenType.VAR);
  		this.put("while", TokenType.WHILE);
  		this.put("then", TokenType.THEN);
  		this.put("begin", TokenType.BEGIN);
  		this.put("real", TokenType.REAL);
                this.put("return", TokenType.RETURN);
 	 }
 }
